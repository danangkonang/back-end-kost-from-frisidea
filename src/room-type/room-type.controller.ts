import {
  Controller,
  Get,
  Param,
  UseGuards,
  Post,
  Req,
  Body,
  Put,
  Delete,
  Query,
} from '@nestjs/common';
import { ApiBearerAuth, ApiImplicitQuery } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';

import {
  CreateRoomTypeDTO,
  UpdateRoomTypeDTO,
  AdditionalCostDTO,
  CostReferenceDTO,
} from './dto';
import { RoomTypeService } from './room-type.service';
import { IQuery } from '../common/interfaces';
import { StatusGuard, RolesGuard } from '../common/guards';
import { Status, Roles } from '../common/decorators';

@Controller()
export class RoomTypeController {
  constructor(private readonly roomTypeService: RoomTypeService) {}

  @Get('kosts/:kost_puid/room-types')
  @ApiImplicitQuery({ name: 'skip', required: false })
  @ApiImplicitQuery({ name: 'take', required: false })
  @ApiImplicitQuery({ name: 'order', required: false })
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'), StatusGuard)
  @Status('completed')
  async getAll(@Query() query: IQuery, @Param('kost_puid') kostId: string) {
    return this.roomTypeService.getAll(query, kostId);
  }

  @Post('kosts/:kost_puid/room-types')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'), StatusGuard, RolesGuard)
  @Status('completed')
  @Roles('person_in_charge', 'super_admin')
  async create(
    @Body() dto: CreateRoomTypeDTO,
    @Req() req: any,
    @Param('kost_puid') kostId: string,
  ) {
    return this.roomTypeService.create(dto, kostId, req);
  }

  @Get('room-types/:type_puid')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'), StatusGuard, RolesGuard)
  @Status('completed')
  @Roles('person_in_charge', 'super_admin')
  async getOne(@Param('type_puid') roomTypeId: string) {
    return this.roomTypeService.getOne(roomTypeId);
  }

  @Put('room-types/:type_puid/update')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'), StatusGuard, RolesGuard)
  @Status('completed')
  @Roles('person_in_charge', 'super_admin')
  async update(
    @Body() dto: UpdateRoomTypeDTO,
    @Param('type_puid') roomTypeId: string,
    @Req() req: any,
  ) {
    return this.roomTypeService.update(dto, roomTypeId, req);
  }

  @Delete('room-types/:type_puid/delete')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'), StatusGuard, RolesGuard)
  @Status('completed')
  @Roles('person_in_charge', 'super_admin')
  async delete(@Param('type_puid') roomTypeId: string) {
    return this.roomTypeService.delete(roomTypeId);
  }

  @Post('room-types/:type_puid/additional-cost')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'), StatusGuard, RolesGuard)
  @Status('completed')
  @Roles('person_in_charge', 'super_admin')
  async addAdditionalCost(
    @Param('type_puid') roomTypeId: string,
    @Body() dto: AdditionalCostDTO,
  ) {
    return this.roomTypeService.addNewAdditionalCosts(roomTypeId, dto);
  }

  @Post('room-types/:type_puid/cost-reference')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'), StatusGuard, RolesGuard)
  @Status('completed')
  @Roles('person_in_charge', 'super_admin')
  async addCostReference(
    @Param('type_puid') roomTypeId: string,
    @Body() dto: CostReferenceDTO,
  ) {
    return this.roomTypeService.addCostReference(roomTypeId, dto);
  }

  @Delete('room-types-addCost/:addCostId/delete')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'), StatusGuard, RolesGuard)
  @Status('completed')
  @Roles('person_in_charge', 'super_admin')
  async deleteAddCost(@Param('addCostId') addCostId: number) {
    return this.roomTypeService.deleteAddCost(addCostId);
  }

  @Delete('room-types-costRef/:costRefId/delete')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'), StatusGuard, RolesGuard)
  @Status('completed')
  @Roles('person_in_charge', 'super_admin')
  async deleteCostRef(@Param('costRefId') costRefId: number) {
    return this.roomTypeService.deleteCostRef(costRefId);
  }
}
