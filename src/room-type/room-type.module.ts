import { Module, NestModule, MiddlewareConsumer } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { RoomTypeController } from './room-type.controller';
import { RoomTypeService } from './room-type.service';

import { Kost } from '../kost/kost.entity';
import {
  RoomType,
  CostReference,
  AdditionalCost,
  Facility,
} from './room-type.entity';
import { Room } from '../room/room.entity';

import { ImagesUploadMiddleware } from '../common/middlewares/images-upload.middleware';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      RoomType,
      Kost,
      Room,
      CostReference,
      AdditionalCost,
      Facility,
    ]),
  ],
  controllers: [RoomTypeController],
  providers: [RoomTypeService],
  exports: [RoomTypeService],
})
export class RoomTypeModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(ImagesUploadMiddleware).forRoutes(RoomTypeController);
  }
}
