import {
  Entity,
  PrimaryGeneratedColumn,
  PrimaryColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
  BeforeInsert,
  OneToMany,
} from 'typeorm';
import * as uuid from 'uuid';

import { Kost } from '../kost/kost.entity';
import { Room } from '../room/room.entity';
import { Contract } from '../contract/contract.entity';
import { Queue } from '../queue/queue.entity';

export enum PeriodType {
  DAILY = 'daily',
  WEEKLY = 'weekly',
  MONTHLY = 'monthly',
  YEARLY = 'yearly',
}

@Entity()
export class RoomType {
  @PrimaryGeneratedColumn()
  id: number;

  @PrimaryColumn()
  public_uid: string;

  @Column()
  name: string;

  @OneToMany(() => Facility, facility => facility.room_type)
  facilities: Facility[];

  @OneToMany(() => AdditionalCost, addtional_cost => addtional_cost.room_type, {
    onDelete: 'SET NULL',
    nullable: true,
  })
  additional_costs: AdditionalCost[];

  @OneToMany(() => CostReference, cost_reference => cost_reference.room_type, {
    onDelete: 'SET NULL',
  })
  cost_references: CostReference[];

  @Column()
  capacity: number;

  @Column('text', {
    array: true,
    nullable: true,
  })
  images: string[];

  @ManyToOne(() => Kost, kost => kost.room_types, { onDelete: 'CASCADE' })
  kost: Kost;

  @OneToMany(() => Room, room => room.room_type, { onDelete: 'SET NULL' })
  rooms: Room[];

  @OneToMany(() => Contract, contract => contract.room_type, {
    onDelete: 'CASCADE',
  })
  contracts: Contract[];

  @OneToMany(() => Queue, queue => queue.room_type)
  queues: Queue[];

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @BeforeInsert()
  generatedPublicUid() {
    this.public_uid = uuid()
      .split('-')
      .pop();
  }
}

@Entity()
export class Facility {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @ManyToOne(() => RoomType, room_type => room_type.facilities, {
    onDelete: 'CASCADE',
  })
  room_type: RoomType;
}

@Entity()
export class AdditionalCost {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  price: number;

  @ManyToOne(() => RoomType, room_type => room_type.additional_costs, {
    onDelete: 'CASCADE',
  })
  room_type: RoomType;
}

@Entity()
export class CostReference {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  price: number;

  @Column({ default: 0 })
  deposit: number;

  @Column({ type: 'enum', enum: PeriodType })
  period_type: PeriodType;

  @ManyToOne(() => RoomType, room_type => room_type.cost_references, {
    onDelete: 'CASCADE',
  })
  room_type: RoomType;
}
