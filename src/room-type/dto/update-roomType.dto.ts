import { ApiModelProperty } from '@nestjs/swagger';
import { IsString, IsArray, IsNumber } from 'class-validator';

export class UpdateRoomTypeDTO {
  @ApiModelProperty()
  @IsString()
  readonly name: string;

  @ApiModelProperty()
  @IsArray()
  readonly facilities: string[];

  @ApiModelProperty()
  @IsNumber()
  readonly capacity: number;
}
