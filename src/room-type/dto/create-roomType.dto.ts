import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsIn, IsEnum, IsNumber, IsString } from 'class-validator';

import { RoomStatus } from '../../room/room.entity';
import { CreateRoomDTO } from '../../room/dto';
import { PeriodType } from '../room-type.entity';

export class CostReferenceDTO {
  @ApiModelProperty()
  @IsNumber()
  readonly price: number;

  @ApiModelProperty()
  readonly deposit: number;

  @ApiModelProperty({ enum: PeriodType })
  @IsEnum(PeriodType)
  readonly periodType: PeriodType;
}

export class AdditionalCostDTO {
  @ApiModelProperty()
  @IsString()
  name: string;

  @ApiModelProperty()
  @IsNumber()
  price: number;
}

export class FacilityDTO {
  @ApiModelProperty()
  @IsString()
  name: string;
}

export class CreateRoomTypeDTO {
  @ApiModelProperty()
  @IsNotEmpty()
  readonly name: string;

  @ApiModelProperty({ type: FacilityDTO, isArray: true })
  @IsNotEmpty()
  readonly facilities: FacilityDTO[];

  @ApiModelProperty()
  @IsNotEmpty()
  readonly capacity: number;

  @ApiModelProperty({ type: CostReferenceDTO, isArray: true })
  @IsNotEmpty()
  readonly costReference: CostReferenceDTO[];

  @ApiModelProperty({ type: AdditionalCostDTO, isArray: true })
  readonly additionalCost: AdditionalCostDTO[];

  @ApiModelProperty({ type: CreateRoomDTO, isArray: true })
  readonly rooms: CreateRoomDTO[];
}
