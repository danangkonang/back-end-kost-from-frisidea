import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { validate } from 'class-validator';

import {
  RoomType,
  AdditionalCost,
  CostReference,
  Facility,
} from './room-type.entity';
import { Kost } from '../kost/kost.entity';
import {
  CreateRoomTypeDTO,
  UpdateRoomTypeDTO,
  AdditionalCostDTO,
  CostReferenceDTO,
  FacilityDTO,
} from './dto';
import { IQuery } from '../common/interfaces';
import { Room } from '../room/room.entity';

@Injectable()
export class RoomTypeService {
  constructor(
    @InjectRepository(RoomType)
    private readonly roomTypeRepository: Repository<RoomType>,
    @InjectRepository(Kost) private readonly kostRepository: Repository<Kost>,
    @InjectRepository(Room) private readonly roomRepository: Repository<Room>,
    @InjectRepository(CostReference)
    private readonly costRefRepository: Repository<CostReference>,
    @InjectRepository(AdditionalCost)
    private readonly addCostRepository: Repository<AdditionalCost>,
    @InjectRepository(Facility)
    private readonly facilityRepository: Repository<Facility>,
  ) {}

  async getAll(query: IQuery, kostId: string) {
    await this.isKostExist(kostId);

    const take = query.take || 5;
    const skip = query.skip || 0;

    const data = await this.roomTypeRepository
      .createQueryBuilder('room_type')
      .leftJoin('room_type.kost', 'kost')
      .leftJoin('room_type.rooms', 'rooms')
      .leftJoinAndSelect('room_type.cost_references', 'cost_references')
      .leftJoinAndSelect('room_type.additional_costs', 'additional_costs')
      .leftJoinAndSelect('room_type.facilities', 'facilities')
      .loadRelationCountAndMap('room_type.rooms_count', 'room_type.rooms')
      .where('kost.public_uid = :kostId', { kostId })
      .select()
      .addSelect(['kost.public_uid', 'kost.name'])
      .addSelect(['rooms.public_uid', 'rooms.number', 'rooms.status'])
      .orderBy('room_type.createdAt', query.order)
      .take(take)
      .skip(skip)
      .getMany();

    try {
      return { status: HttpStatus.OK, data };
    } catch (error) {
      throw new HttpException({ error }, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  async getOne(roomTypeId: string) {
    const data = await this.roomTypeRepository
      .createQueryBuilder('room_type')
      .leftJoin('room_type.kost', 'kost')
      .leftJoin('room_type.rooms', 'rooms')
      .leftJoinAndSelect('room_type.cost_references', 'cost_references')
      .leftJoinAndSelect('room_type.additional_costs', 'additional_costs')
      .where('room_type.public_uid = :roomTypeId', { roomTypeId })
      .getOne();

    if (!data) {
      throw new HttpException(
        { erorr: 'Room Type was not found' },
        HttpStatus.NOT_FOUND,
      );
    }
    try {
      return { status: HttpStatus.OK, data };
    } catch (error) {
      throw new HttpException({ error }, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  async create(dto: CreateRoomTypeDTO, kostId: string, req: any) {
    const kost = await this.isKostExist(kostId);

    const newRoomType = new RoomType();

    const costReference = dto.costReference.map(item => {
      const newCostReference = new CostReference();
      newCostReference.price = item.price;
      newCostReference.deposit = item.deposit;
      newCostReference.period_type = item.periodType;
      return this.costRefRepository.save(newCostReference);
    });

    const rooms = dto.rooms
      ? dto.rooms.map(item => {
          const newRoom = new Room();
          newRoom.number = item.number;
          newRoom.status = item.status;
          newRoom.kost_puid = kostId;
          return this.roomRepository.save(newRoom);
        })
      : [];

    const addCost = dto.additionalCost
      ? dto.additionalCost.map(item => {
          const newAdditionalCost = new AdditionalCost();
          newAdditionalCost.name = item.name;
          newAdditionalCost.price = item.price;
          return this.addCostRepository.save(newAdditionalCost);
        })
      : [];

    const images = req.files ? req.files.map((item: any) => item.location) : [];
    const facilities = dto.facilities.map(item => {
      const newFacility = new Facility();
      newFacility.name = item.name;
      return this.facilityRepository.save(newFacility);
    });

    newRoomType.name = dto.name;
    newRoomType.facilities = await Promise.all(facilities);
    newRoomType.capacity = dto.capacity;
    newRoomType.cost_references = await Promise.all(costReference);
    newRoomType.additional_costs = await Promise.all(addCost);
    newRoomType.rooms = await Promise.all(rooms);
    newRoomType.images = images;
    newRoomType.kost = kost;

    try {
      await validate(newRoomType);
      const savedRoomType = await this.roomTypeRepository.save(newRoomType);

      return { status: HttpStatus.CREATED, data: savedRoomType };
    } catch (error) {
      throw new HttpException({ error }, HttpStatus.BAD_REQUEST);
    }
  }

  async update(dto: UpdateRoomTypeDTO, roomTypeId: string, req: any) {
    const roomType = await this.roomTypeRepository.findOne({
      public_uid: roomTypeId,
    });

    if (!roomType) {
      throw new HttpException(
        { error: 'Room Type was not found' },
        HttpStatus.NOT_FOUND,
      );
    }

    const images = req.files
      ? req.files.map((item: any) => item.location)
      : roomType.images;

    roomType.name = dto.name;
    roomType.capacity = dto.capacity;
    roomType.images = images;

    try {
      await validate(roomType);
      const updatedRoomType = await this.roomTypeRepository.save(roomType);
      return { status: HttpStatus.OK, data: updatedRoomType };
    } catch (error) {
      throw new HttpException({ error }, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  async addNewFacility(roomTypeId: string, dto: FacilityDTO) {
    const roomType = await this.findByPuid(roomTypeId);

    const newFacility = new Facility();

    newFacility.name = dto.name;
    newFacility.room_type = roomType;

    try {
      await this.facilityRepository.save(newFacility);
      return { status: HttpStatus.CREATED, data: 'Facility added' };
    } catch (error) {
      throw new HttpException({ error }, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  async addNewAdditionalCosts(roomTypeId: string, dto: AdditionalCostDTO) {
    const roomType = await this.findByPuid(roomTypeId);

    const newAdditionalCost = new AdditionalCost();

    newAdditionalCost.name = dto.name;
    newAdditionalCost.price = dto.price;
    newAdditionalCost.room_type = roomType;

    try {
      await this.addCostRepository.save(newAdditionalCost);
      return { status: HttpStatus.CREATED, data: 'Additional Cost added' };
    } catch (error) {
      throw new HttpException({ error }, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  async addCostReference(roomTypeId: string, dto: CostReferenceDTO) {
    const roomType = await this.findByPuid(roomTypeId);

    const newCostReference = new CostReference();

    newCostReference.deposit = dto.deposit;
    newCostReference.period_type = dto.periodType;
    newCostReference.price = dto.price;
    newCostReference.room_type = roomType;

    try {
      await this.costRefRepository.save(newCostReference);
      return { status: HttpStatus.CREATED, data: 'Cost added' };
    } catch (error) {
      throw new HttpException({ error }, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  async deleteFacility(facilityId: number) {
    try {
      const data = await this.facilityRepository.findOne(facilityId);
      await this.facilityRepository.remove(data);
      return { status: HttpStatus.NO_CONTENT, data: 'Deleted' };
    } catch (error) {
      throw new HttpException({ error }, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  async deleteAddCost(addCostId: number) {
    try {
      const data = await this.addCostRepository.findOne(addCostId);
      await this.addCostRepository.remove(data);
      return { status: HttpStatus.NO_CONTENT, data: 'Deleted' };
    } catch (error) {
      throw new HttpException({ error }, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  async deleteCostRef(costRefId: number) {
    try {
      const data = await this.costRefRepository.findOne(costRefId);
      await this.costRefRepository.remove(data);
      return { status: HttpStatus.NO_CONTENT, data: 'Deleted' };
    } catch (error) {
      throw new HttpException({ error }, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  async delete(roomTypeId: string) {
    const roomType = await this.roomTypeRepository.findOne({
      public_uid: roomTypeId,
    });

    if (!roomType) {
      throw new HttpException(
        { error: 'Room Type was not found' },
        HttpStatus.NOT_FOUND,
      );
    }

    try {
      await this.roomTypeRepository.remove(roomType);
      return {
        status: HttpStatus.NO_CONTENT,
        message: 'Data has been successfully deleted',
      };
    } catch (error) {
      throw new HttpException({ error }, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  async kostFindById(kostId: string, req: any) {
    try {
      return this.kostRepository
        .createQueryBuilder('kost')
        .leftJoin('kost.users', 'users')
        .where('kost.public_uid = :kostId', { kostId })
        .andWhere('users.public_uid = :userId', {
          userId: req.user.public_uid,
        })
        .getOne();
    } catch (error) {
      throw new HttpException({ error }, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  async isKostExist(kostId: string) {
    const data = await this.kostRepository.findOne({ public_uid: kostId });

    if (!data) {
      throw new HttpException(
        { error: 'No Kost was found' },
        HttpStatus.NOT_FOUND,
      );
    }

    return data;
  }

  async findByPuid(roomTypeId: string) {
    const roomType = await this.roomTypeRepository
      .createQueryBuilder('room_type')
      .leftJoinAndSelect('room_type.cost_references', 'cost_references')
      .leftJoinAndSelect('room_type.additional_costs', 'additional_costs')
      .where('room_type.public_uid = :roomTypeId', { roomTypeId })
      .getOne();

    if (!roomType) {
      throw new HttpException(
        { error: 'No Room Type was found' },
        HttpStatus.NOT_FOUND,
      );
    }

    return roomType;
  }
}
