import { Module, MiddlewareConsumer, NestModule } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { DB } from './config';
import { UserModule } from './user/user.module';
import { AuthModule } from './auth/auth.module';
import { KostModule } from './kost/kost.module';
import { RoomTypeModule } from './room-type/room-type.module';
import { RoomModule } from './room/room.module';
import { ContractModule } from './contract/contract.module';
import { CustomerModule } from './customer/customer.module';
import { QueueModule } from './queue/queue.module';
import { ImageUploadMiddleware } from './common/middlewares/image-upload.middleware';
import { APP_INTERCEPTOR } from '@nestjs/core';
import { NotificationsInterceptor } from './common/interceptors/notifications.interceptor';
import { TransactionModule } from './transaction/transaction.module';
import { ContractDailyReminder } from './common/services/cron-jobs';
import { ScheduleModule } from 'nest-schedule';
import { HourlyContractReminder } from './common/services/cron-jobs';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: DB.HOST,
      port: DB.PORT,
      username: DB.USERNAME,
      password: DB.PASSWORD,
      database: DB.NAME,
      entities: [__dirname + '/**/*.entity{.ts,.js}'],
      synchronize: true,
    }),
    ScheduleModule.register({}),
    UserModule,
    AuthModule,
    KostModule,
    RoomTypeModule,
    RoomModule,
    ContractModule,
    CustomerModule,
    QueueModule,
    TransactionModule,
  ],
  controllers: [AppController],
  providers: [
    {
      provide: APP_INTERCEPTOR,
      useClass: NotificationsInterceptor,
    },
    ContractDailyReminder,
    HourlyContractReminder,
  ],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(ImageUploadMiddleware).forRoutes(AppController);
  }
}
