import {
  Controller,
  Post,
  Body,
  Put,
  UseGuards,
  Req,
  Param,
} from '@nestjs/common';
import { ApiBearerAuth } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';

import {
  LoginDTO,
  RegisterOneDTO,
  ForgotPasswordDTO,
  RegisterTwoDTO,
  RegisterThreeDTO,
  CompleteProfileDTO,
} from './dto';
import { AuthService } from './auth.service';
import { ChangePasswordDTO } from './dto/change-password.dto';
import { StatusGuard } from '../common/guards/status.guard';
import { Status } from '../common/decorators';
@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('login')
  async login(@Body() loginData: LoginDTO) {
    return this.authService.login(loginData);
  }

  @Post('register/one')
  async registerPartOne(@Body() registerData: RegisterOneDTO) {
    return this.authService.registerPartOne(registerData);
  }

  @Post('register/two')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  async registerPartTwo(@Body() registerData: RegisterTwoDTO, @Req() req: any) {
    return this.authService.registerPartTwo(registerData, req.user.public_uid);
  }

  @Post('register/three')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  async registerPartThree(
    @Body() registerData: RegisterThreeDTO,
    @Req() req: any,
  ) {
    return this.authService.registerPartThree(
      registerData,
      req.user.public_uid,
    );
  }

  @Post('complete-profile')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'), StatusGuard)
  @Status('activated')
  async registerPartFour(@Body() dto: CompleteProfileDTO, @Req() req: any) {
    return this.authService.completeProfile(dto, req);
  }

  @Post('new-otp')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  async generateNewOTP(@Req() req: any) {
    return this.authService.generateNewOTP(req.user.public_uid);
  }

  @Post('forgot-password')
  async forgotPassword(@Body() forgotPasswordData: ForgotPasswordDTO) {
    return this.authService.forgotPassword(forgotPasswordData);
  }

  @Put('forgot-password/:token')
  async forgotPasswordChange(
    @Body() changePassword: ChangePasswordDTO,
    @Param('token') token: string,
  ) {
    return this.authService.forgotPasswordChanage(changePassword, token);
  }
}
