import { Module, MiddlewareConsumer } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { TypeOrmModule } from '@nestjs/typeorm';

import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { JwtStrategy } from './jwt.strategy';
import { JWT } from '../config';

import { User } from '../user/user.entity';
import { Kost } from '../kost/kost.entity';
import { AuthLog } from './auth-log.entity';
import { BusinessInfo } from '../common/entities/business-info.entity';

import { ImageUploadMiddleware } from '../common/middlewares/image-upload.middleware';

@Module({
  imports: [
    TypeOrmModule.forFeature([User, Kost, AuthLog, BusinessInfo]),
    PassportModule.register({ defaultStrategy: 'jwt' }),
    JwtModule.register({
      secret: JWT.secret,
    }),
  ],
  controllers: [AuthController],
  providers: [AuthService, JwtStrategy],
})
export class AuthModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(ImageUploadMiddleware).forRoutes(AuthController);
  }
}
