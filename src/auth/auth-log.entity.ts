import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  BeforeInsert,
  BeforeUpdate,
  OneToOne,
  JoinTable,
} from 'typeorm';
import { User } from '../user/user.entity';

@Entity()
export class AuthLog {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  status: string;

  @Column()
  timestamp: Date;

  @Column({ default: 0 })
  failed_attemp: number;

  @OneToOne(() => User, user => user.auth_log, { onDelete: 'CASCADE' })
  user: User;

  failedLogin() {
    this.failed_attemp = this.failed_attemp += 1;
    this.status = 'failed login';
  }

  successfulLogin() {
    this.failed_attemp = 0;
    this.status = 'successful login';
  }

  @BeforeInsert()
  @BeforeUpdate()
  getTime() {
    this.timestamp = new Date();
  }
}
