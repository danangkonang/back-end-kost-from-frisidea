import { IsNotEmpty, IsEnum } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

import { Gender } from '../../user/user.entity';

import { Type, VisitHours, PetsPolicy } from '../../kost/kost.entity';

export class CompleteProfileDTO {
  @ApiModelProperty()
  @IsNotEmpty()
  readonly full_name: string;

  @ApiModelProperty({ enum: Gender })
  @IsEnum(Gender)
  readonly gender: Gender;

  @ApiModelProperty()
  @IsNotEmpty()
  readonly sub_domain: string;

  @ApiModelProperty()
  @IsNotEmpty()
  readonly address: string;

  @ApiModelProperty()
  @IsNotEmpty()
  readonly department: string;

  @ApiModelProperty()
  @IsNotEmpty()
  readonly kost_name: string;

  @ApiModelProperty()
  @IsNotEmpty()
  readonly kost_address: string;

  @ApiModelProperty({ enum: Type })
  @IsEnum(Type)
  readonly kost_type: Type;

  @ApiModelProperty({ enum: VisitHours })
  @IsEnum(VisitHours)
  readonly visit_hours: VisitHours;

  @ApiModelProperty()
  readonly max_hours_visit: string;

  @ApiModelProperty({ enum: PetsPolicy })
  @IsEnum(PetsPolicy)
  readonly pets_policy: PetsPolicy;

  @ApiModelProperty()
  readonly deadline_hours: number;

  @ApiModelProperty()
  readonly deadline_days: number;

  @ApiModelProperty()
  readonly queue_limit: number;

  @ApiModelProperty()
  readonly hourly_contract_reminder: number;

  @ApiModelProperty()
  readonly daily_contract_reminder: number;

  @ApiModelProperty()
  readonly hourly_deadline_reminder: number;

  @ApiModelProperty()
  readonly daily_deadline_reminder: number;

  @ApiModelProperty()
  readonly receipt_upload: boolean;

  @ApiModelProperty()
  readonly deposit: boolean;
}
