import { MinLength, MaxLength } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class ChangePasswordDTO {
  @ApiModelProperty()
  @MinLength(8)
  @MaxLength(16)
  readonly password: string;

  @ApiModelProperty()
  @MinLength(8)
  @MaxLength(16)
  readonly confirmPassword: string;
}
