import { ApiModelProperty } from '@nestjs/swagger';
import { IsEmail } from 'class-validator';

export class ForgotPasswordDTO {
  @ApiModelProperty()
  @IsEmail()
  email: string;
}
