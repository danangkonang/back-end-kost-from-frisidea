import { IsEmail, IsMobilePhone, MaxLength } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class RegisterOneDTO {
  @ApiModelProperty()
  @IsEmail({}, { message: 'Please enter the right email' })
  readonly email: string;

  @ApiModelProperty()
  @IsMobilePhone('id-ID', { message: 'Please enter the right phone number' })
  @MaxLength(12, { message: 'Phone number must not exceed 12 numbers' })
  readonly phone_number: string;
}
