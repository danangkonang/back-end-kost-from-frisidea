import { IsNotEmpty, IsNumber } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class RegisterTwoDTO {
  @ApiModelProperty()
  @IsNotEmpty({ message: '$property is required' })
  @IsNumber()
  readonly otp: number;
}
