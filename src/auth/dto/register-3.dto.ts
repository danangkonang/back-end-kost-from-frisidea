import { MinLength, MaxLength } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class RegisterThreeDTO {
  @ApiModelProperty()
  @MinLength(8, { message: 'Password must be at least 8 characters' })
  @MaxLength(16, { message: 'Password must not exceed 16 characters' })
  readonly password: string;

  @ApiModelProperty()
  @MinLength(8, { message: 'Confirm Password must be at least 8 characters' })
  @MaxLength(16, { message: 'Confirm Password must not exceed 16 characters' })
  readonly confirmPassword: string;
}
