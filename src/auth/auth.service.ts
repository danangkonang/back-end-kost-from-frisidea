import { JwtService } from '@nestjs/jwt';
import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { validate } from 'class-validator';

import {
  LoginDTO,
  RegisterOneDTO,
  ForgotPasswordDTO,
  RegisterTwoDTO,
  RegisterThreeDTO,
  CompleteProfileDTO,
} from './dto';

import SendGrid from '../common/services/sendgrid';
import { User, Status } from '../user/user.entity';
import { Kost } from '../kost/kost.entity';
import { AuthLog } from './auth-log.entity';
import { jwtPayload } from './interface/jwtPayload.interface';
import { ChangePasswordDTO } from './dto/change-password.dto';
import { BusinessInfo } from '../common/entities/business-info.entity';

import { createSubDomain } from '../common/services/godaddy';

@Injectable()
export class AuthService {
  constructor(
    @InjectRepository(User) private readonly userRepository: Repository<User>,
    @InjectRepository(Kost) private readonly kostRepository: Repository<Kost>,
    @InjectRepository(AuthLog)
    private readonly logRepository: Repository<AuthLog>,
    @InjectRepository(BusinessInfo)
    private readonly infoRepository: Repository<BusinessInfo>,
    private readonly jwtService: JwtService,
  ) {}

  async login(dto: LoginDTO): Promise<object> {
    const { email, password } = dto;
    const user = await this.userRepository
      .createQueryBuilder('user')
      .leftJoinAndSelect('user.auth_log', 'auth_log')
      .where('user.email = :email', { email })
      .getOne();

    if (!user) {
      throw new HttpException(
        { error: 'User not found' },
        HttpStatus.NOT_FOUND,
      );
    }

    switch (user.status) {
      case 'not_activated': {
        throw new HttpException(
          { error: 'Please activate your account' },
          HttpStatus.FORBIDDEN,
        );
      }

      case 'disabled': {
        throw new HttpException(
          { error: 'Your account had been disabled, please contact admin' },
          HttpStatus.FORBIDDEN,
        );
      }

      default: {
        const authLog = await this.logRepository.findOne(user.auth_log.id);
        const isValid = await user.comparePassword(password);

        if (authLog.failed_attemp >= 3) {
          throw new HttpException(
            {
              error:
                'Failed to login more than 3 times, please use forget password in order to login',
            },
            HttpStatus.FORBIDDEN,
          );
        } else if (!isValid) {
          authLog.failedLogin();
          await this.logRepository.save(authLog);
          throw new HttpException(
            { error: 'Invalid password' },
            HttpStatus.UNAUTHORIZED,
          );
        } else {
          const payload: jwtPayload = {
            userId: user.public_uid,
          };
          authLog.successfulLogin();
          await this.logRepository.save(authLog);
          const token = await this.jwtService.sign(payload);
          return { status: HttpStatus.OK, token };
        }
      }
    }
  }

  async registerPartOne(dto: RegisterOneDTO): Promise<object> {
    const { email, phone_number } = dto;

    await this.checkAvailableUser(email, phone_number);

    let authLog = new AuthLog();
    authLog.status = 'registered';

    let newUser = new User();
    newUser.email = email;
    newUser.phone_number = phone_number;
    newUser.auth_log = authLog;

    try {
      await validate(newUser);
      await this.logRepository.save(authLog);
      const user = await this.userRepository.save(newUser);
      await SendGrid.sendOTP(user.email, user.otp);

      const payload = { userId: user.public_uid, otp: user.otp };
      const token = this.jwtService.sign(payload);
      return { status: HttpStatus.OK, message: 'Check your email', token };
    } catch (error) {
      console.log(error);
      throw new HttpException({ message: error }, HttpStatus.BAD_REQUEST);
    }
  }

  async registerPartTwo(dto: RegisterTwoDTO, userId: string) {
    const user = await this.userRepository.findOne({ public_uid: userId });

    const timeNow = Date.now();
    const groundTime = Date.parse(`${user.updatedAt}`);
    const timeLimit = groundTime + 15 * 60 * 1000;

    if (!user) {
      throw new HttpException(
        { message: 'No user found' },
        HttpStatus.NOT_FOUND,
      );
    } else if (user.otp !== dto.otp) {
      throw new HttpException(
        { message: 'Invalid OTP' },
        HttpStatus.BAD_REQUEST,
      );
    } else if (timeNow > timeLimit) {
      throw new HttpException(
        { message: 'OTP is expired' },
        HttpStatus.BAD_REQUEST,
      );
    } else {
      user.otp = 1;

      try {
        await validate(user);
        await this.userRepository.save(user);
        return { status: HttpStatus.OK, data: 'OTP Success' };
      } catch (error) {
        throw new HttpException({ error }, HttpStatus.INTERNAL_SERVER_ERROR);
      }
    }
  }

  async registerPartThree(dto: RegisterThreeDTO, userId: string) {
    const user = await this.userRepository.findOne({ public_uid: userId });
    const validPassword = dto.password.match(
      /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])/,
    );

    if (!user) {
      throw new HttpException(
        { message: 'No user found' },
        HttpStatus.NOT_FOUND,
      );
    } else if (user.otp !== 1) {
      throw new HttpException(
        { message: 'Please confirm your OTP first' },
        HttpStatus.FORBIDDEN,
      );
    } else if (!validPassword) {
      throw new HttpException(
        {
          message:
            'Password must contain 1 Uppercase, 1 Special char and 1 Number',
        },
        HttpStatus.BAD_REQUEST,
      );
    } else if (dto.password !== dto.confirmPassword) {
      throw new HttpException(
        {
          message: "Password doesn't match confirm password",
        },
        HttpStatus.BAD_REQUEST,
      );
    } else {
      user.password = dto.password;
      user.status = Status.ACTIVATED;

      try {
        await validate(user);
        await user.hashPassword();
        await SendGrid.welcomeMesage(user.email);
        await this.userRepository.save(user);
        return {
          status: HttpStatus.OK,
          data: 'User now activated, you can login now',
        };
      } catch (error) {
        throw new HttpException({ error }, HttpStatus.INTERNAL_SERVER_ERROR);
      }
    }
  }

  async completeProfile(dto: CompleteProfileDTO, req: any) {
    const user = await this.userRepository.findOne({
      public_uid: req.user.public_uid,
    });

    if (!user) {
      throw new HttpException(
        { message: 'No user found' },
        HttpStatus.NOT_FOUND,
      );
    }

    const newKost = new Kost();

    // USER PROFILE
    user.full_name = dto.full_name;
    user.gender = dto.gender;
    user.address = dto.address;
    user.department = dto.department;
    user.status = Status.COMPLETED;
    if (req.file) {
      user.ktp_img = req.file.location;
    }
    // USER's BUSINESS PROFILE
    newKost.name = dto.kost_name;
    newKost.address = dto.kost_address;
    newKost.type = dto.kost_type;
    newKost.users = [user];
    newKost.visit_hours = dto.visit_hours;
    newKost.max_hours_visit = dto.max_hours_visit;
    newKost.pets_policy = dto.pets_policy;

    try {
      // await createSubDomain(dto.sub_domain);
      await validate({ user, newKost });
      const createdUser = await this.userRepository.save(user);
      const createdKost = await this.kostRepository.save(newKost);

      const newInfo = new BusinessInfo();
      newInfo.employees = [createdUser];
      newInfo.kosts = [createdKost];
      newInfo.sub_domain = dto.sub_domain;
      newInfo.hourly_deadline_reminder = dto.hourly_deadline_reminder;
      newInfo.daily_deadline_reminder = dto.daily_deadline_reminder;
      newInfo.daily_contract_reminder = dto.daily_contract_reminder;
      newInfo.hourly_contract_reminder = dto.hourly_contract_reminder;
      newInfo.receipt_upload = dto.receipt_upload;
      newInfo.deposit = dto.deposit;

      await this.infoRepository.save(newInfo);

      return { status: HttpStatus.OK, message: "User's data now completed" };
    } catch (error) {
      throw new HttpException({ error }, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  async generateNewOTP(userId: string) {
    const user = await this.userRepository.findOne({ public_uid: userId });

    if (!user) {
      throw new HttpException(
        { message: 'No user found' },
        HttpStatus.NOT_FOUND,
      );
    }

    try {
      user.generateOTP();
      await this.userRepository.save(user);
      await SendGrid.sendOTP(user.email, user.otp);
      return {
        status: HttpStatus.OK,
        data: 'New OTP has been sent to your email',
      };
    } catch (error) {
      throw new HttpException({ error }, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  async forgotPassword(dto: ForgotPasswordDTO) {
    const { email } = dto;

    const user = await this.userRepository.findOne({ email });

    if (!user) {
      throw new HttpException(
        { error: 'No user was found' },
        HttpStatus.NOT_FOUND,
      );
    } else {
      const payload: jwtPayload = { userId: user.public_uid };
      const token = this.jwtService.sign(payload);
      try {
        await SendGrid.forgotPassword(user.email, token);
        return { status: HttpStatus.OK, message: 'Check your email' };
      } catch (error) {
        throw new HttpException({ error }, HttpStatus.INTERNAL_SERVER_ERROR);
      }
    }
  }

  async forgotPasswordChanage(dto: ChangePasswordDTO, token: string) {
    const { password, confirmPassword } = dto;

    const payload = this.jwtService.verify(token);

    const user = await this.userRepository.findOne(
      { public_uid: payload.userId },
      { relations: ['auth_log'] },
    );

    const validPassword = password.match(
      /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])/,
    );

    if (!validPassword) {
      throw new HttpException(
        {
          error:
            'Password must contain 1 Uppercase, 1 Special char and 1 Number',
        },
        HttpStatus.BAD_REQUEST,
      );
    } else if (password !== confirmPassword) {
      throw new HttpException(
        { error: 'Password did not match the confirm password' },
        HttpStatus.BAD_REQUEST,
      );
    } else {
      try {
        const authLog = await this.logRepository.findOne(user.auth_log.id);
        authLog.failed_attemp = 0;
        user.password = password;
        await user.hashPassword();
        await this.logRepository.save(authLog);
        await this.userRepository.save(user);
        return { status: HttpStatus.OK, message: 'Password has been changed' };
      } catch (error) {
        console.log(error);
        throw new HttpException({ error }, HttpStatus.INTERNAL_SERVER_ERROR);
      }
    }
  }

  async checkAvailableUser(email: string, phoneNumber: string) {
    const user = await this.userRepository
      .createQueryBuilder('user')
      .where('user.phone_number = :phone_number', { phone_number: phoneNumber })
      .orWhere('user.email = :email', { email })
      .getOne();

    if (user) {
      const error = {
        error: 'Phone number or email had already been taken',
      };
      throw new HttpException(
        { message: 'Validation error', error },
        HttpStatus.BAD_REQUEST,
      );
    }

    return true;
  }

  async validateUser(payload: jwtPayload): Promise<any> {
    try {
      return this.userRepository
        .createQueryBuilder('user')
        .leftJoin('user.kosts', 'kosts')
        .leftJoinAndSelect('user.business_info', 'business_info')
        .where('user.public_uid = :userId', { userId: payload.userId })
        .select()
        .addSelect(['kosts.public_uid'])
        .getOne();
    } catch (error) {
      throw new HttpException({ error }, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
