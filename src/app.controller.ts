import { Controller, Get, Post, Req, Body } from '@nestjs/common';

import { seed } from './seed';
@Controller()
export class AppController {
  @Get()
  async getHello() {
    await seed();
    return { data: 'Hello, World' };
  }

  @Post('/test-upload')
  testUpload(@Body() dto: any, @Req() req: any) {
    const dummy = {
      name: dto.name,
      description: dto.description,
    };
    console.log(dto);

    return req.file;
  }
}
