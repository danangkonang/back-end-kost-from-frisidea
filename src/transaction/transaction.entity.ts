import {
  Entity,
  PrimaryGeneratedColumn,
  PrimaryColumn,
  Column,
  BeforeInsert,
  CreateDateColumn,
} from 'typeorm';
import * as uuid from 'uuid';

@Entity()
export class ContractPayment {
  @PrimaryGeneratedColumn()
  id: number;

  @PrimaryColumn()
  public_uid: string;

  @Column()
  kost_puid: string;

  @Column()
  invoice_number: string;

  @Column()
  payment_date: Date;

  @Column()
  customer_name: string;

  @Column()
  phone_number: string;

  @Column()
  email: string;

  @Column()
  room_type_name: string;

  @Column()
  room_number: number;

  @Column()
  contract_start: Date;

  @Column()
  contract_end: Date;

  @Column()
  durations: number;

  @Column()
  contract_type: string;

  @Column()
  total_payment: number;

  @Column()
  total_recieved: number;

  @Column({ nullable: true })
  receipt_payment: string;

  @CreateDateColumn()
  createdAt: Date;

  @BeforeInsert()
  generatedPublicUid() {
    this.public_uid = uuid()
      .split('-')
      .pop();
  }
}

@Entity()
export class DepositPayment {
  @PrimaryGeneratedColumn()
  id: number;

  @PrimaryColumn()
  public_uid: string;

  @Column()
  kost_puid: string;

  @Column()
  customer_puid: string;

  @Column()
  contract_puid: string;

  @Column()
  invoice_number: string;

  @Column()
  customer_name: string;

  @Column()
  payment_date: Date;

  @Column()
  total_deposit: number;

  @Column({ nullable: true })
  receipt_payment: string;

  @CreateDateColumn()
  createdAt: Date;

  @BeforeInsert()
  generatedPublicUid() {
    this.public_uid = uuid()
      .split('-')
      .pop();
  }
}

@Entity()
export class DepositWithdraw {
  @PrimaryGeneratedColumn()
  id: number;

  @PrimaryColumn()
  public_uid: string;

  @Column()
  invoice_number: string;

  @Column()
  kost_puid: string;

  @Column()
  customer_name: string;

  @Column()
  withdraw_date: Date;

  @Column()
  total_deposit: number;

  @Column()
  total_withdraw: number;

  @Column()
  receipt_withdraw: string;

  @CreateDateColumn()
  createdAt: Date;

  @BeforeInsert()
  generatedPublicUid() {
    this.public_uid = uuid()
      .split('-')
      .pop();
  }
}
