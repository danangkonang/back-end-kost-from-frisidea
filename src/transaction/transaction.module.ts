import { Module } from '@nestjs/common';
import { TransactionController } from './transaction.controller';
import { TransactionService } from './transaction.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import {
  ContractPayment,
  DepositPayment,
  DepositWithdraw,
} from './transaction.entity';
import { ContractModule } from '../contract/contract.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      ContractPayment,
      DepositPayment,
      DepositWithdraw,
    ]),
    ContractModule,
  ],
  controllers: [TransactionController],
  providers: [TransactionService],
})
export class TransactionModule {}
