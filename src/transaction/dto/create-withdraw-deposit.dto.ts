import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNumber } from 'class-validator';

export class CreateDepositWithdrawDTO {
  @ApiModelProperty({ type: 'string', format: 'date-time' })
  @IsNotEmpty()
  withdrawDate: Date;

  @ApiModelProperty()
  @IsNotEmpty()
  @IsNumber()
  totalWithdraw: number;
}
