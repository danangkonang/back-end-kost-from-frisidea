import { ApiModelProperty } from '@nestjs/swagger';
import { IsString, IsNotEmpty } from 'class-validator';

export class CreateContractPaymentDTO {
  @ApiModelProperty({ type: 'string', format: 'date-time' })
  @IsNotEmpty()
  paymentDate: Date;

  @ApiModelProperty()
  @IsNotEmpty()
  totalReceived: number;
}
