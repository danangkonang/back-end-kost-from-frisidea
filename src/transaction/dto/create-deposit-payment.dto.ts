import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class CreateDepositPaymentDTO {
  @ApiModelProperty({ type: 'string', format: 'date-time' })
  @IsNotEmpty()
  paymentDate: Date;
}
