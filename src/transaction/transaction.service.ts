import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { validate } from 'class-validator';
import * as moment from 'moment';

import {
  ContractPayment,
  DepositPayment,
  DepositWithdraw,
} from './transaction.entity';
import { ContractService } from '../contract/contract.service';
import {
  CreateContractPaymentDTO,
  CreateDepositPaymentDTO,
  CreateDepositWithdrawDTO,
} from './dto';
import { IQuery } from '../common/interfaces';

@Injectable()
export class TransactionService {
  constructor(
    @InjectRepository(ContractPayment)
    private readonly contractPaymentRepository: Repository<ContractPayment>,
    @InjectRepository(DepositPayment)
    private readonly depositPaymentRepository: Repository<DepositPayment>,
    @InjectRepository(DepositWithdraw)
    private readonly depositWithdrawRepository: Repository<DepositWithdraw>,
    private readonly contractService: ContractService,
  ) {}

  async getAllContractPayment(query: IQuery, kostId: string) {
    const take = query.take || 5;
    const skip = query.skip || 0;

    const data = await this.contractPaymentRepository
      .createQueryBuilder('contract_payment')
      .where('contract_payment.kost_puid = :kostId', { kostId })
      .orderBy('contract_payment.createdAt', query.order)
      .take(take)
      .skip(skip)
      .getMany();

    return { status: HttpStatus.OK, data };
  }

  async getAllDepositPayment(query: IQuery, kostId: string) {
    const take = query.take || 5;
    const skip = query.skip || 0;

    const data = await this.depositPaymentRepository
      .createQueryBuilder('deposit_payment')
      .where('deposit_payment.kost_puid = :kostId', { kostId })
      .orderBy('room_type.createdAt', query.order)
      .take(take)
      .skip(skip)
      .getMany();

    return { status: HttpStatus.OK, data };
  }

  async getAllDepositWithdraw(query: IQuery, kostId: string) {
    const take = query.take || 5;
    const skip = query.skip || 0;

    const data = await this.depositWithdrawRepository
      .createQueryBuilder('deposit_withdraw')
      .where('deposit_withdraw.kost_puid = :kostId', { kostId })
      .orderBy('room_type.createdAt', query.order)
      .take(take)
      .skip(skip)
      .getMany();

    return { status: HttpStatus.OK, data };
  }

  async getContractPayment(contractPaymentId: string) {
    const data = await this.contractPaymentRepository.findOne({
      public_uid: contractPaymentId,
    });

    if (!data) {
      throw new HttpException(
        { error: 'No Invoice was found' },
        HttpStatus.NOT_FOUND,
      );
    }

    return { status: HttpStatus.OK, data };
  }

  async getDepositPayment(depositPaymentId: string) {
    const data = await this.depositPaymentRepository.findOne({
      public_uid: depositPaymentId,
    });

    if (!data) {
      throw new HttpException(
        { error: 'No Invoice was found' },
        HttpStatus.NOT_FOUND,
      );
    }

    return { status: HttpStatus.OK, data };
  }

  async getDepositWithdraw(depositWithdrawId: string) {
    const data = await this.depositWithdrawRepository.findOne({
      public_uid: depositWithdrawId,
    });

    if (!data) {
      throw new HttpException(
        { error: 'No Invoice was found' },
        HttpStatus.NOT_FOUND,
      );
    }

    return { status: HttpStatus.OK, data };
  }

  async createContractPayment(
    dto: CreateContractPaymentDTO,
    contractId: string,
  ) {
    const contract = await this.contractService.findByPuid(contractId);
    const customer = contract.customers.find(item => item.type === 'main');
    const invoiceNumber = await this.generateInvoiceNumber(
      contract.kost_puid,
      'MC',
    );

    const newContarctPayment = new ContractPayment();

    let totalCosts = contract.cost_references * contract.durations;

    if (contract.additional_costs) {
      let total = 0;
      let costs = contract.additional_costs;

      for (let i = 0; i < costs.length; i++) {
        total += costs[i].price;
      }

      totalCosts = totalCosts + total;
    }

    newContarctPayment.total_payment = totalCosts;

    newContarctPayment.invoice_number = invoiceNumber;
    newContarctPayment.kost_puid = contract.kost_puid;
    newContarctPayment.customer_name = customer.name;
    newContarctPayment.phone_number = customer.phone_number;
    newContarctPayment.email = customer.email;
    newContarctPayment.contract_start = contract.contract_start;
    newContarctPayment.contract_end = contract.contract_end;
    newContarctPayment.room_type_name = contract.room_type.name;
    newContarctPayment.room_number = contract.room.number;
    newContarctPayment.durations = contract.durations;
    newContarctPayment.contract_type = contract.period_type;

    newContarctPayment.payment_date = dto.paymentDate;
    newContarctPayment.total_recieved = dto.totalReceived;

    try {
      await validate(newContarctPayment);
      const data = await this.contractPaymentRepository.save(
        newContarctPayment,
      );
      await this.contractService.paysContract(contract);
      return { status: HttpStatus.CREATED, data };
    } catch (error) {
      throw new HttpException({ error }, HttpStatus.BAD_REQUEST);
    }
  }

  async createDepositPayment(dto: CreateDepositPaymentDTO, contractId: string) {
    const contract = await this.contractService.findByPuid(contractId);
    const customer = contract.customers.find(item => item.type === 'main');
    const invoiceNumber = await this.generateInvoiceNumber(
      contract.kost_puid,
      'DP',
    );

    const newDepositPayment = new DepositPayment();

    newDepositPayment.invoice_number = invoiceNumber;
    newDepositPayment.kost_puid = contract.kost_puid;
    newDepositPayment.customer_name = customer.name;
    newDepositPayment.total_deposit = contract.deposit;
    newDepositPayment.payment_date = dto.paymentDate;
    newDepositPayment.contract_puid = contract.public_uid;
    newDepositPayment.customer_puid = customer.public_uid;

    try {
      await validate(newDepositPayment);
      const data = await this.depositPaymentRepository.save(newDepositPayment);
      return { status: HttpStatus.CREATED, data };
    } catch (error) {
      throw new HttpException({ error }, HttpStatus.BAD_REQUEST);
    }
  }

  async createDepositWithdraw(
    dto: CreateDepositWithdrawDTO,
    depositId: string,
  ) {
    const deposit = await this.depositPaymentRepository
      .createQueryBuilder('deposit')
      .where('depost.public_uid = :depositId', { depositId })
      .getOne();
    const invoiceNumber = await this.generateInvoiceNumber(
      deposit.kost_puid,
      'WDP',
    );

    const newDepositWithdraw = new DepositWithdraw();

    newDepositWithdraw.invoice_number = invoiceNumber;
    newDepositWithdraw.kost_puid = deposit.kost_puid;
    newDepositWithdraw.customer_name = deposit.customer_name;
    newDepositWithdraw.total_deposit = deposit.total_deposit;
    newDepositWithdraw.total_withdraw = dto.totalWithdraw;
    newDepositWithdraw.withdraw_date = dto.withdrawDate;

    try {
      await validate(newDepositWithdraw);
      const data = await this.depositWithdrawRepository.save(
        newDepositWithdraw,
      );
      return { status: HttpStatus.CREATED, data };
    } catch (error) {
      throw new HttpException({ error }, HttpStatus.BAD_REQUEST);
    }
  }

  async generateInvoiceNumber(kostId: string, type: string) {
    const date = moment().format('DDMMYY');
    let digits = [0, 0, 0, 0];

    function plusOne(digits: number[]) {
      for (var i = digits.length - 1; i >= 0; i--) {
        digits[i]++;
        if (digits[i] > 9) {
          digits[i] = 0;
        } else {
          return digits;
        }
      }
      digits.unshift(1);
      return digits;
    }

    let lastInvoice: ContractPayment | DepositPayment | DepositWithdraw;

    if (type === 'MC') {
      lastInvoice = await this.contractPaymentRepository
        .createQueryBuilder('invoice')
        .where('invoice.kost_puid = :kostId', { kostId })
        .orderBy('invoice.createdAt', 'DESC')
        .getOne();
    } else if (type === 'DP') {
      lastInvoice = await this.depositPaymentRepository
        .createQueryBuilder('invoice')
        .where('invoice.kost_puid = :kostId', { kostId })
        .orderBy('invoice.createdAt', 'DESC')
        .getOne();
    } else {
      lastInvoice = await this.depositWithdrawRepository
        .createQueryBuilder('invoice')
        .where('invoice.kost_puid = :kostId', { kostId })
        .orderBy('invoice.createdAt', 'DESC')
        .getOne();
    }

    if (!lastInvoice) {
      const number = plusOne(digits);
      return `INV/${type}/${date}/${number.join('')}`;
    }

    let invoiceDate = lastInvoice.invoice_number
      .split('/')
      .slice(2, 3)
      .toString();
    let lastNumber = lastInvoice.invoice_number.split('/').pop();

    if (invoiceDate === date) {
      let nums = lastNumber.split('').map(i => parseInt(i));
      let plusOneNums = plusOne(nums);
      return `INV/${type}/${date}/${plusOneNums.join('')}`;
    } else {
      const number = plusOne(digits);
      return `INV/${type}/${date}/${number.join('')}`;
    }
  }
}
