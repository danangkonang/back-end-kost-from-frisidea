import {
  Controller,
  Post,
  UseGuards,
  Param,
  Body,
  Get,
  Query,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiImplicitQuery } from '@nestjs/swagger';

import {
  CreateContractPaymentDTO,
  CreateDepositPaymentDTO,
  CreateDepositWithdrawDTO,
} from './dto';
import { StatusGuard } from '../common/guards';
import { TransactionService } from './transaction.service';
import { IQuery } from '../common/interfaces';
import { Status } from '../common/decorators';

@Controller()
export class TransactionController {
  constructor(private readonly transactionService: TransactionService) {}

  @Get('kosts/:kost_puid/contract-payment')
  @ApiBearerAuth()
  @ApiImplicitQuery({ name: 'skip', required: false })
  @ApiImplicitQuery({ name: 'take', required: false })
  @ApiImplicitQuery({ name: 'order', required: false })
  @UseGuards(AuthGuard('jwt'), StatusGuard)
  @Status('completed')
  async getAllContractPayment(
    @Query() query: IQuery,
    @Param('kost_puid') kostId: string,
  ) {
    return this.transactionService.getAllContractPayment(query, kostId);
  }

  @Get('kosts/:kost_puid/deposit-payment')
  @ApiImplicitQuery({ name: 'skip', required: false })
  @ApiImplicitQuery({ name: 'take', required: false })
  @ApiImplicitQuery({ name: 'order', required: false })
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'), StatusGuard)
  @Status('completed')
  async getAllDepositPayment(
    @Query() query: IQuery,
    @Param('kost_puid') kostId: string,
  ) {
    return this.transactionService.getAllDepositPayment(query, kostId);
  }

  @Get('kosts/:kost_puid/deposit-withdraw')
  @ApiImplicitQuery({ name: 'skip', required: false })
  @ApiImplicitQuery({ name: 'take', required: false })
  @ApiImplicitQuery({ name: 'order', required: false })
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'), StatusGuard)
  @Status('completed')
  async getAllDepositWithdraw(
    @Query() query: IQuery,
    @Param('kost_puid') kostId: string,
  ) {
    return this.transactionService.getAllDepositWithdraw(query, kostId);
  }

  @Post('contracts/:contract_puid/contract-payment')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'), StatusGuard)
  @Status('completed')
  async createContractPayment(
    @Param('contract_puid') contractId: string,
    @Body() dto: CreateContractPaymentDTO,
  ) {
    return this.transactionService.createContractPayment(dto, contractId);
  }

  @Post('contracts/:contract_puid/deposit-payment')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'), StatusGuard)
  @Status('completed')
  async createDepositPayment(
    @Param('contract_puid') contractId: string,
    @Body() dto: CreateDepositPaymentDTO,
  ) {
    return this.transactionService.createDepositPayment(dto, contractId);
  }

  @Post('deposits/:deposit_puid/withdraw-deposit')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'), StatusGuard)
  @Status('completed')
  async createDepositWithdraw(
    @Param('deposit_puid') depositId: string,
    @Body() dto: CreateDepositWithdrawDTO,
  ) {
    return this.transactionService.createDepositWithdraw(dto, depositId);
  }

  @Get('contract-payment/:puid')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'), StatusGuard)
  @Status('completed')
  async getContractPayment(@Param('puid') puid: string) {
    return this.transactionService.getContractPayment(puid);
  }

  @Get('deposit-payment/:puid')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'), StatusGuard)
  @Status('completed')
  async getDepositPayment(@Param('puid') puid: string) {
    return this.transactionService.getDepositPayment(puid);
  }

  @Get('deposit-withdraw/:puid')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'), StatusGuard)
  @Status('completed')
  async getDepositWithdraw(@Param('puid') puid: string) {
    return this.transactionService.getDepositWithdraw(puid);
  }
}
