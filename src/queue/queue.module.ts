import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { QueueController } from './queue.controller';
import { QueueService } from './queue.service';
import { Queue } from './queue.entity';

import { RoomModule } from '../room/room.module';
import { CustomerModule } from '../customer/customer.module';

@Module({
  imports: [TypeOrmModule.forFeature([Queue]), RoomModule, CustomerModule],
  controllers: [QueueController],
  providers: [QueueService],
})
export class QueueModule {}
