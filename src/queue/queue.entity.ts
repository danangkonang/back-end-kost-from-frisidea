import {
  Entity,
  PrimaryGeneratedColumn,
  PrimaryColumn,
  Column,
  OneToOne,
  JoinColumn,
  ManyToOne,
  Generated,
  BeforeInsert,
  BeforeUpdate,
} from 'typeorm';
import * as uuid from 'uuid';
import * as moment from 'moment';

import { Customer } from '../customer/customer.entity';
import { Room } from '../room/room.entity';
import { RoomType } from '../room-type/room-type.entity';
import { HttpException, HttpStatus } from '@nestjs/common';

export enum PeriodType {
  DAILY = 'daily',
  WEEKLY = 'weekly',
  MONTHLY = 'monthly',
  YEARLY = 'yearly',
}

@Entity()
export class Queue {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  @Generated('increment')
  queue_no: number;

  @PrimaryColumn()
  public_uid: string;

  @Column()
  kost_puid: string;

  @Column({ type: 'enum', enum: PeriodType })
  period_type: PeriodType;

  @Column()
  booking_start: Date;

  @Column()
  contract_start: Date;

  @Column()
  contract_end: Date;

  @Column()
  durations: number;

  @OneToOne(() => Customer, customer => customer.queue, {
    onDelete: 'SET NULL',
  })
  @JoinColumn()
  customer: Customer;

  @ManyToOne(() => Room, room => room.queues, { onDelete: 'CASCADE' })
  room: Room;

  @ManyToOne(() => RoomType, roomType => roomType.queues)
  room_type: RoomType;

  @BeforeInsert()
  generatedPublicUid() {
    this.public_uid = uuid()
      .split('-')
      .pop();
  }

  @BeforeInsert()
  @BeforeUpdate()
  generateDuration() {
    let start = {
      day: moment(this.contract_start).dayOfYear(),
      week: moment(this.contract_start).week(),
      month: moment(this.contract_start).month() + 1,
      year: moment(this.contract_start).year(),
    };

    let end = {
      day: moment(this.contract_end).dayOfYear(),
      week: moment(this.contract_end).week(),
      month: moment(this.contract_end).month() + 1,
      year: moment(this.contract_end).year(),
    };

    if (this.period_type === PeriodType.DAILY) {
      if (end.year > start.year) {
        let days = moment([start.year, 11, 31]).dayOfYear();
        let newEnd = end.day + days;
        this.durations = newEnd - start.day;
      }
      this.durations = end.day - start.day;
    } else if (this.period_type === PeriodType.WEEKLY) {
      if (end.year > start.year) {
        let weeks = moment([start.year, 11, 31]).week();
        let newEnd = end.week + weeks;
        this.durations = newEnd - start.week;
      }
      this.durations = end.week - start.week;
    } else if (this.period_type === PeriodType.MONTHLY) {
      if (end.year > start.year) {
        let newEnd = end.month + 12;
        this.durations = newEnd - start.month;
      }
      this.durations = end.month - start.month;
    } else if (this.period_type === PeriodType.YEARLY) {
      this.durations = end.year - start.year;
    } else {
      throw new HttpException(
        { error: 'Fatal Error' },
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }
}
