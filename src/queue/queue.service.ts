import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { Queue } from './queue.entity';

import { RoomService } from '../room/room.service';
import { CustomerService } from '../customer/customer.service';
import { CreateQueueDTO, UpdateQueueDTO } from './dto';
@Injectable()
export class QueueService {
  constructor(
    @InjectRepository(Queue) private queueRepository: Repository<Queue>,
    private readonly roomService: RoomService,
    private readonly customerService: CustomerService,
  ) {}

  async getAll(kostId: string) {
    try {
      const data = await this.queueRepository
        .createQueryBuilder('queue')
        .leftJoin('queue.room', 'room')
        .leftJoin('queue.customer', 'customer')
        .leftJoin('queue.room_type', 'room_type')
        .where('room.kost_puid = :kostId', { kostId })
        .select()
        .addSelect([
          'customer.public_uid',
          'customer.name',
          'customer.email',
          'customer.phone_number',
        ])
        .addSelect([
          'room_type.public_uid',
          'room_type.name',
          'room_type.capacity',
        ])
        .addSelect(['room.public_uid', 'room.number'])
        .getMany();

      return { status: HttpStatus.OK, data };
    } catch (error) {
      console.log(error);
      throw new HttpException({ error }, HttpStatus.BAD_REQUEST);
    }
  }

  async getOne(roomId: string) {
    try {
      const data = await this.queueRepository
        .createQueryBuilder('queue')
        .leftJoinAndSelect('queue.room', 'room')
        .leftJoinAndSelect('queue.room_type', 'room_type')
        .where('room.public_uid = :roomId', { roomId })
        .getOne();

      if (!data) {
        throw new HttpException(
          { error: 'No queue was found' },
          HttpStatus.BAD_REQUEST,
        );
      }

      return { status: HttpStatus.OK, data };
    } catch (error) {
      throw new HttpException({ error }, HttpStatus.BAD_REQUEST);
    }
  }

  async create(dto: CreateQueueDTO, roomId: string, limit: number) {
    const room = await this.roomService.findByPuid(roomId);
    const customer = await this.customerService.findByPuid(dto.customerId);

    if (room.queues.length === limit) {
      throw new HttpException(
        { error: 'You can not add more queue, try change your configuration' },
        HttpStatus.BAD_REQUEST,
      );
    }

    if (dto.checkInDate === dto.checkOutDate) {
      throw new HttpException({ error: 'Bad Request' }, HttpStatus.BAD_REQUEST);
    }

    const validPeriod = room.room_type.cost_references.find(item => {
      return item.period_type === dto.periodType;
    });

    if (!validPeriod) {
      throw new HttpException(
        { error: 'Period type not found' },
        HttpStatus.BAD_REQUEST,
      );
    }
    const queue = new Queue();

    queue.room = room;
    queue.customer = customer;
    queue.period_type = dto.periodType;
    queue.booking_start = dto.bookingStart;
    queue.contract_start = dto.checkInDate;
    queue.contract_end = dto.checkOutDate;
    queue.kost_puid = room.kost_puid;
    queue.room_type = room.room_type;

    try {
      const data = await this.queueRepository.save(queue);
      return { status: HttpStatus.CREATED, data };
    } catch (error) {
      throw new HttpException({ error }, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  async update(dto: UpdateQueueDTO, queueId: string) {
    const queue = await this.findByPuid(queueId);
    const customer = await this.customerService.findByPuid(dto.customerId);

    queue.customer = customer;
    queue.period_type = dto.periodType;
    queue.contract_start = dto.checkInDate;
    queue.contract_end = dto.checkOutDate;

    try {
      const data = await this.queueRepository.save(queue);
      return { status: HttpStatus.OK, data };
    } catch (error) {
      throw new HttpException({ error }, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  async delete(queueId: string) {
    const queue = await this.findByPuid(queueId);

    try {
      await this.queueRepository.remove(queue);

      return { status: HttpStatus.NO_CONTENT, data: 'Queue has been deleted' };
    } catch (error) {
      throw new HttpException({ error }, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  async findByPuid(queueId: string) {
    const queue = await this.queueRepository
      .createQueryBuilder('queue')
      .where('queue.public_uid = :queueId', { queueId })
      .getOne();

    if (!queue) {
      throw new HttpException(
        { error: 'No Queue was found' },
        HttpStatus.NOT_FOUND,
      );
    }

    return queue;
  }
}
