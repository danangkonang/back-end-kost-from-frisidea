import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsEnum } from 'class-validator';
import { PeriodType } from '../queue.entity';

export class CreateQueueDTO {
  @ApiModelProperty()
  @IsNotEmpty()
  readonly customerId: string;

  @ApiModelProperty({ enum: PeriodType })
  @IsEnum(PeriodType)
  readonly periodType: PeriodType;

  @ApiModelProperty({ type: 'string', format: 'date-time' })
  @IsNotEmpty()
  readonly bookingStart: Date;

  @ApiModelProperty({ type: 'string', format: 'date-time' })
  @IsNotEmpty()
  readonly checkInDate: Date;

  @ApiModelProperty({ type: 'string', format: 'date-time' })
  @IsNotEmpty()
  readonly checkOutDate: Date;
}
