import {
  Controller,
  Post,
  UseGuards,
  Param,
  Get,
  Body,
  Put,
  Delete,
  Req,
} from '@nestjs/common';
import { ApiBearerAuth } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';

import { QueueService } from './queue.service';
import { StatusGuard } from '../common/guards/status.guard';
import { CreateQueueDTO, UpdateQueueDTO } from './dto';
import { Status } from '../common/decorators';

@Controller()
export class QueueController {
  constructor(private readonly queueService: QueueService) {}

  @Get('kosts/:kost_puid/queues')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'), StatusGuard)
  async getAll(@Param('kost_puid') kostId: string) {
    return this.queueService.getAll(kostId);
  }

  @Get('rooms/:room_puid/queue')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'), StatusGuard)
  @Status('completed')
  async getOne(@Param('room_puid') roomId: string) {
    return this.queueService.getOne(roomId);
  }

  @Post('rooms/:room_puid/queue')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'), StatusGuard)
  @Status('completed')
  async create(
    @Param('room_puid') roomId: string,
    @Body() dto: CreateQueueDTO,
    @Req() req: any,
  ) {
    const { queue_limit } = req.user.business_info;
    return this.queueService.create(dto, roomId, queue_limit);
  }

  @Put('queue/:queue_puid/update')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'), StatusGuard)
  @Status('completed')
  async update(
    @Param('queue_puid') queueId: string,
    @Body() dto: UpdateQueueDTO,
  ) {
    return this.queueService.update(dto, queueId);
  }

  @Delete('queue/:queue_puid/delete')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'), StatusGuard)
  @Status('completed')
  async delete(@Param('queue_puid') queueId: string) {
    return this.queueService.delete(queueId);
  }
}
