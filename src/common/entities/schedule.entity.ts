import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToOne,
  JoinColumn,
} from 'typeorm';
import { Contract } from '../../contract/contract.entity';

export enum ReminderType {
  CONTRACT = 'contract',
  DEADLINE = 'deadline',
}

@Entity()
export class Reminder {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  trigger_date: string;

  @Column()
  trigger_hours: string;

  @Column()
  period_type: string;

  @Column({ type: 'enum', enum: ReminderType })
  type: ReminderType;

  @OneToOne(() => Contract, contract => contract.reminder, {
    onDelete: 'CASCADE',
  })
  @JoinColumn()
  contract: Contract;
}
