import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToMany,
  PrimaryColumn,
  BeforeInsert,
  CreateDateColumn,
} from 'typeorm';
import * as uuid from 'uuid';

import { User } from '../../user/user.entity';
import { Kost } from '../../kost/kost.entity';
import { BusinessNotification } from './notification.entity';

@Entity()
export class BusinessInfo {
  @PrimaryGeneratedColumn()
  id: number;

  @PrimaryColumn()
  public_uid: string;

  @Column()
  sub_domain: string;

  @Column({ default: 3 })
  hourly_deadline_reminder: number;

  @Column({ default: 3 })
  daily_deadline_reminder: number;

  @Column({ default: 10 })
  queue_limit: number;

  @Column({ default: true })
  receipt_upload: boolean;

  @Column({ default: 3 })
  hourly_contract_reminder: number;

  @Column({ default: 3 })
  daily_contract_reminder: number;

  @Column({ default: true })
  deposit: boolean;

  @OneToMany(() => User, user => user.business_info, { onDelete: 'SET NULL' })
  employees: User[];

  @OneToMany(() => Kost, kost => kost.business_info, { onDelete: 'SET NULL' })
  kosts: Kost[];

  @OneToMany(
    () => BusinessNotification,
    notification => notification.business_info,
  )
  notifications: BusinessNotification[];

  @CreateDateColumn()
  createdAt: Date;

  @BeforeInsert()
  generatePublicUid() {
    this.public_uid = uuid()
      .split('-')
      .pop();
  }
}
