import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToOne,
  ManyToOne,
  CreateDateColumn,
} from 'typeorm';
import { BusinessInfo } from './business-info.entity';

@Entity()
export class BusinessNotification {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  contract_puid: string;

  @Column()
  message: string;

  @ManyToOne(() => BusinessInfo, businessInfo => businessInfo.notifications)
  business_info: BusinessInfo;

  @CreateDateColumn()
  createdAt: Date;
}
