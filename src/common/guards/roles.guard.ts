import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';

import { Role } from '../../user/user.entity';
import { Reflector } from '@nestjs/core';
@Injectable()
export class RolesGuard implements CanActivate {
  constructor(private readonly reflector: Reflector) {}

  canActivate(context: ExecutionContext): boolean {
    const roles = this.reflector.get<string[]>('roles', context.getHandler());
    if (!roles) {
      return true;
    }
    const request = context.switchToHttp().getRequest();
    const user = request.user;
    const userRoles = new Array(user.role);
    const hasRole = () =>
      userRoles.some((role: Role) => !!roles.find(item => item === role));

    return user && user.role && hasRole();
  }
}
