import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { Reflector } from '@nestjs/core';

import { Status } from '../../user/user.entity';
@Injectable()
export class StatusGuard implements CanActivate {
  constructor(private readonly reflector: Reflector) {}

  canActivate(context: ExecutionContext): boolean {
    const status = this.reflector.get<string[]>('status', context.getHandler());
    if (!status) {
      return true;
    }
    const request = context.switchToHttp().getRequest();
    const user = request.user;
    const userStatus = new Array(user.status);
    const hasStatus = () =>
      userStatus.some((stat: Status) => !!status.find(item => item === stat));

    return user && user.status && hasStatus();
  }
}
