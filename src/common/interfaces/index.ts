import { KostStatus } from 'src/kost/kost.entity';

enum Order {
  ASC = 'ASC',
  DESC = 'DESC',
}

export interface IQuery {
  take: number;
  skip: number;
  order: Order;
}

export interface IKostQuery {
  take: number;
  skip: number;
  order: Order;
  status: KostStatus;
}

export interface IRoomQuery {
  customerEmail: string;
  roomId: string;
}

export interface IEmployeesQuery {
  search: string;
  order: Order;
}
