import {
  Injectable,
  NestInterceptor,
  ExecutionContext,
  CallHandler,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

export interface Response<T> {
  notification: string;
  data: T;
}

@Injectable()
export class NotificationsInterceptor<T>
  implements NestInterceptor<T, Response<T>> {
  intercept(
    context: ExecutionContext,
    next: CallHandler,
  ): Observable<Response<T>> {
    const { user } = context.switchToHttp().getRequest();
    if (user && user.status !== 'completed') {
      return next.handle().pipe(
        map(data => {
          data.notification = 'Please complete your account';
          return data;
        }),
      );
    } else {
      return next.handle().pipe(map(data => data));
    }
  }
}
