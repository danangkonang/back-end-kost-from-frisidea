import {
  Injectable,
  NestInterceptor,
  ExecutionContext,
  CallHandler,
  HttpException,
  HttpStatus,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { getRepository } from 'typeorm';

import { Room } from '../../room/room.entity';

@Injectable()
export class RoomCheckInterceptor implements NestInterceptor {
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    const { user, params } = context.switchToHttp().getRequest();
    const roomRepository = getRepository(Room);

    roomRepository
      .createQueryBuilder('room')
      .where('room.public_uid = :roomId', { roomId: params.room_puid })
      .getRawOne()
      .then(res => {
        const permitted = user.kosts.find(
          (item: any) => item.public_uid === res.room_kost_puid,
        );
        if (!permitted) {
          throw new HttpException(
            { error: 'Forbidden Error' },
            HttpStatus.FORBIDDEN,
          );
        }
      })
      .catch(err => {
        console.log(err);
      });

    return next.handle();
  }
}
