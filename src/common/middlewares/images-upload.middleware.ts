import { Injectable, NestMiddleware } from '@nestjs/common';
import { Request, Response } from 'express';
import { uploads } from '../services/aws-s3';

@Injectable()
export class ImagesUploadMiddleware implements NestMiddleware {
  use(req: Request, res: Response, next: () => void) {
    try {
      uploads(req, res, (error) => {
        if (error) {
          return res
            .status(400)
            .json({ error: `Failed to upload image file: ${error}` });
        }
        next();
      });
    } catch (error) {
      return res
        .status(500)
        .json({ error: `Failed to upload image file: ${error}` });
    }
  }
}
