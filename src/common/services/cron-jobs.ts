import { InjectSchedule, Schedule } from 'nest-schedule';
import { Injectable, OnModuleInit } from '@nestjs/common';
import { getRepository } from 'typeorm';
import * as moment from 'moment';

import { BusinessNotification } from '../entities/notification.entity';
import {
  Contract,
  ContractStatus,
  PeriodType,
} from '../../contract/contract.entity';
import { Reminder, ReminderType } from '../entities/schedule.entity';
import { BusinessInfo } from '../entities/business-info.entity';

enum InOrNot {
  IN = 'IN',
  NOT = 'NOT IN',
}

const findReminder = async (inOrNot: InOrNot) => {
  const date = moment().format('DDMMYY');

  const reminders = await getRepository(Reminder)
    .createQueryBuilder('reminder')
    .leftJoinAndSelect('reminder.contract', 'contract')
    .innerJoinAndMapOne(
      'contract.business_info',
      BusinessInfo,
      'business_info',
      'business_info.public_uid = contract.business_puid',
    )
    .where(`reminder.period_type ${inOrNot} (:type)`, {
      type: PeriodType.DAILY,
    })
    .andWhere('reminder.trigger_date = :date', { date })
    .getMany();

  return reminders;
};

@Injectable()
export class ContractDailyReminder implements OnModuleInit {
  constructor(@InjectSchedule() private readonly schedule: Schedule) {}

  onModuleInit(): any {
    this.schedule.scheduleCronJob(
      'daily_contract_reminder',
      '0 0 * * *',
      async function() {
        const reminders = await findReminder(InOrNot.NOT);

        if (reminders.length > 0) {
          const createNotifications = reminders.map((reminder: any) => {
            const newNotification = new BusinessNotification();

            if (reminder.type === ReminderType.CONTRACT) {
              newNotification.business_info = reminder.contract.businessInfo;
              newNotification.contract_puid = reminder.contract.public_uid;
              newNotification.message =
                'Contract has changed status to END SOON';

              reminder.contract.status = ContractStatus.END_SOON;
              return (
                getRepository(Contract).save(reminder.contract) &&
                getRepository(BusinessNotification).save(newNotification)
              );
            } else {
              newNotification.business_info = reminder.businessInfo;
              newNotification.contract_puid = reminder.contract.public_uid;
              newNotification.message =
                'Payment for a contract has met its deadline';

              return getRepository(BusinessNotification).save(newNotification);
            }
          });

          await Promise.all(createNotifications);
        }

        return false;
      },
    );
  }
}

export class HourlyContractReminder implements OnModuleInit {
  constructor(@InjectSchedule() private readonly schedule: Schedule) {}

  onModuleInit(): any {
    this.schedule.scheduleCronJob(
      'hourly_contract_reminder',
      '0 * * * *',
      async function() {
        const hours = moment().format('HH');
        const temp = await findReminder(InOrNot.IN);
        const reminders = temp.filter(item => item.trigger_hours === hours);

        if (reminders.length > 0) {
          const createNotifications = reminders.map((reminder: any) => {
            const newNotification = new BusinessNotification();

            if (reminder.type === ReminderType.CONTRACT) {
              newNotification.business_info = reminder.contract.businessInfo;
              newNotification.contract_puid = reminder.contract.public_uid;
              newNotification.message =
                'Contract has changed status to END SOON';

              reminder.contract.status = ContractStatus.END_SOON;
              return (
                getRepository(Contract).save(reminder.contract) &&
                getRepository(BusinessNotification).save(newNotification)
              );
            } else {
              newNotification.business_info = reminder.businessInfo;
              newNotification.contract_puid = reminder.contract.public_uid;
              newNotification.message =
                'Payment for a contract has met its deadline';

              return getRepository(BusinessNotification).save(newNotification);
            }
          });
          await Promise.all(createNotifications);
        }

        return false;
      },
    );
  }
}
