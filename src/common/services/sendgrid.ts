import * as client from '@sendgrid/client';
import { HttpException, HttpStatus } from '@nestjs/common';

import { SENDGRID } from '../../config';

class SendGrid {
  constructor(public APIKey?: string) {
    this.APIKey = SENDGRID.apiKey;
  }

  async sendOTP(email: string, otp: number) {
    client.setApiKey(this.APIKey);

    try {
      await client.request({
        url: '/v3/mail/send',
        method: 'POST',
        body: {
          personalizations: [
            {
              to: [{ email: email }],
              dynamic_template_data: {
                name: 'User',
                otp: otp,
              },
            },
          ],
          from: { email: 'noreply@kost.id' },
          reply_to: { email: 'noreply@kost.id' },
          template_id: 'd-de156fcd957547f193d8e3c22314b61a',
        },
      });
    } catch (error) {
      throw new HttpException({ error }, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  async sendLoginCredentials(email: string, password: string) {
    client.setApiKey(this.APIKey);

    try {
      await client.request({
        url: '/v3/mail/send',
        method: 'POST',
        body: {
          personalizations: [
            {
              to: [{ email: email }],
              dynamic_template_data: {
                email,
                password: password,
              },
            },
          ],
          from: { email: 'noreply@kost.id' },
          reply_to: { email: 'noreply@kost.id' },
          template_id: 'd-68d05ce7902b4365be69fc197649a186',
        },
      });
    } catch (error) {
      throw new HttpException({ error }, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  async welcomeMesage(email: string) {
    client.setApiKey(this.APIKey);

    try {
      await client.request({
        url: '/v3/mail/send',
        method: 'POST',
        body: {
          personalizations: [
            {
              to: [{ email: email }],
              dynamic_template_data: {
                email,
              },
            },
          ],
          from: { email: 'noreply@kost.id' },
          reply_to: { email: 'noreply@kost.id' },
          template_id: 'd-cdde7efcc0e7435d9cf8d6180a679ddc',
        },
      });
    } catch (error) {
      throw new HttpException({ error }, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  async forgotPassword(email: string, token: string) {
    client.setApiKey(this.APIKey);

    try {
      await client.request({
        url: '/v3/mail/send',
        method: 'POST',
        body: {
          personalizations: [
            {
              to: [{ email: email }],
              dynamic_template_data: {
                token,
              },
            },
          ],
          from: { email: 'noreply@kost.id' },
          reply_to: { email: 'noreply@kost.id' },
          template_id: 'd-806e570dc86c4b6abfd0523696d412e0',
        },
      });
    } catch (error) {
      throw new HttpException({ error }, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}

export default new SendGrid();
