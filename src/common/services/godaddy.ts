import * as request from 'request-promise-native';

import { GDD } from '../../config';

async function getDomains() {
  return await request.get(
    'https://api.godaddy.com/v1/domains/qiutee.com/records/',
    {
      headers: {
        Authorization: `sso-key ${GDD.key}:${GDD.secret}`,
        accept: 'application/json',
      },
    },
  );
}

async function createSubDomain(name: string) {
  const options = {
    headers: {
      Authorization: `sso-key ${GDD.key}:${GDD.secret}`,
      accept: 'application/json',
    },
    body: [{ data: '127.0.0.1', name, type: 'A' }],
    json: true,
  };

  return await request.patch(
    'https://api.godaddy.com/v1/domains/qiutee.com/records/',
    options,
  );
}

export { getDomains, createSubDomain };
