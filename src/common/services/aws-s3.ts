import * as AWS from 'aws-sdk';
import * as multer from 'multer';
import * as multerS3 from 'multer-s3';

const s3 = new AWS.S3({
  s3ForcePathStyle: true,
  accessKeyId: 'ACCESS_KEY_ID',
  secretAccessKey: 'SECRET_ACCESS_KEY',
  endpoint: 'http://192.168.99.100:4569',
});

const upload = multer({
  fileFilter: function(req, file, cb) {
    if (!file.originalname.match(/\.(jpg|jpeg|png)$/)) {
      return cb(new Error('Only image files are allowed!'), false);
    }
    cb(null, true);
  },
  limits: {
    fileSize: 2 * 1024 * 1024,
  },
  storage: multerS3({
    s3: s3,
    bucket: 'my_bucket',
    acl: 'public-read',
    key: function(request, file, cb) {
      cb(null, `${Date.now().toString()}-${file.originalname}`);
    },
  }),
}).single('image');

const uploads = multer({
  fileFilter: function(req, file, cb) {
    if (!file.originalname.match(/\.(jpg|jpeg|png)$/)) {
      return cb(new Error('Only image files are allowed!'), false);
    }
    cb(null, true);
  },
  storage: multerS3({
    s3: s3,
    bucket: 'my_bucket',
    acl: 'public-read',
    key: function(request, file, cb) {
      cb(null, `${Date.now().toString()}-${file.originalname}`);
    },
  }),
}).array('images', 5);

export { upload, uploads };
