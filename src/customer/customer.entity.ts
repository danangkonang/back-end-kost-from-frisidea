import {
  Entity,
  PrimaryGeneratedColumn,
  PrimaryColumn,
  Column,
  BeforeInsert,
  ManyToOne,
  OneToOne,
  Unique,
} from 'typeorm';
import * as uuid from 'uuid';

import { Kost } from '../kost/kost.entity';
import { Contract } from '../contract/contract.entity';
import { Queue } from '../queue/queue.entity';

export enum CustomerType {
  MAIN = 'main',
  ROOMMATE = 'roommate',
}

export enum Gender {
  MALE = 'male',
  FEMALE = 'female',
}

export enum Occupation {
  WORKER = 'worker',
  STUDENT = 'student',
}

export enum Religion {
  ISLAM = 'islam',
  CHRISTIAN = 'christian',
  HINDU = 'hindu',
  BUDDHA = 'buddha',
  OTHER = 'other',
}

export enum CustomerStatus {
  ACTIVE = 'active',
  BLOCKED = 'blocked',
  INACTIVE = 'inactive',
}

@Entity()
@Unique(['email'])
export class Customer {
  @PrimaryGeneratedColumn()
  id: number;

  @PrimaryColumn()
  public_uid: string;

  @Column()
  name: string;

  @Column()
  email: string;

  @Column({ type: 'enum', enum: Gender })
  gender: Gender;

  @Column()
  birth_date: Date;

  @Column({ type: 'enum', enum: Occupation })
  occupation: Occupation;

  @Column()
  phone_number: string;

  @Column({ type: 'enum', enum: Religion })
  religion: Religion;

  @Column()
  emegrency_name: string;

  @Column()
  emergency_phone: string;

  @Column('text')
  address: string;

  @Column({ type: 'enum', enum: CustomerType })
  type: CustomerType;

  @Column({ nullable: true })
  ktp_img: string;

  @Column({ nullable: true })
  kk_img: string;

  @Column()
  comment: string;

  @Column({
    type: 'enum',
    enum: CustomerStatus,
    default: CustomerStatus.ACTIVE,
  })
  status: CustomerStatus;

  @ManyToOne(() => Kost, kost => kost.customer, { onDelete: 'CASCADE' })
  kost: Kost;

  @ManyToOne(() => Contract, contract => contract.customers, {
    onDelete: 'SET NULL',
  })
  contract: Contract;

  @OneToOne(() => Queue, queue => queue.customer, { onDelete: 'SET NULL' })
  queue: Queue;

  @BeforeInsert()
  generatePublicUid() {
    this.public_uid = uuid()
      .split('-')
      .pop();
  }
}
