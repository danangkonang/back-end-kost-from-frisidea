import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { Customer, CustomerStatus, CustomerType } from './customer.entity';

import { CreateCustomerDTO, UpdateCustomerDTO } from './dto';
import { KostService } from '../kost/kost.service';
import { IQuery } from '../common/interfaces';
import { ContractStatus } from '../contract/contract.entity';

@Injectable()
export class CustomerService {
  constructor(
    @InjectRepository(Customer)
    private readonly customerRepository: Repository<Customer>,
    private readonly kostService: KostService,
  ) {}

  async getCountStatus(kostId: string) {
    let active = 0;
    let inactive = 0;
    let blocked = 0;

    const data = await this.customerRepository
      .createQueryBuilder('customer')
      .leftJoin('customer.kost', 'kost')
      .where('kost.public_uid = :kostId', { kostId })
      .getMany();

    for (let i = 0; i < data.length; i++) {
      if (data[i].status === CustomerStatus.ACTIVE) {
        active++;
      } else if (data[i].status === CustomerStatus.INACTIVE) {
        inactive++;
      } else {
        blocked++;
      }
    }

    return { status: HttpStatus.OK, data: { active, inactive, blocked } };
  }

  async getAll(kostId: string, query: IQuery) {
    const take = query.take || 10;
    const skip = query.skip || 0;

    try {
      const data = await this.customerRepository
        .createQueryBuilder('customer')
        .leftJoin('customer.kost', 'kost')
        .leftJoin('customer.contract', 'contract')
        .where('kost.public_uid = :kostId', { kostId })
        .select()
        .addSelect([
          'contract.public_uid',
          'contract.status',
          'contract.contract_start',
          'contract.contract_end',
        ])
        .orderBy('customer.name', query.order)
        .skip(skip)
        .take(take)
        .getMany();

      if (!data) {
        throw new HttpException(
          { error: 'No Kost was found' },
          HttpStatus.NOT_FOUND,
        );
      }

      return { status: HttpStatus.OK, data };
    } catch (error) {
      console.log(error);
      throw new HttpException({ error }, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  async getOne(customerId: string) {
    const data = await this.customerRepository
      .createQueryBuilder('customer')
      .leftJoinAndSelect('customer.kost', 'kost')
      .leftJoinAndSelect('customer.reservation', 'reservation')
      .where('customer.public_uid = :customerId', { customerId })
      .getOne();

    if (!data) {
      throw new HttpException(
        { error: 'No customer was found' },
        HttpStatus.NOT_FOUND,
      );
    }

    return { status: HttpStatus.OK, data };
  }

  async create(kostId: string, dto: CreateCustomerDTO, req: any) {
    await this.isCustomerExist(dto.email);

    const kost = await this.kostService.findById(kostId, req);

    const customer = new Customer();
    customer.name = dto.name;
    customer.email = dto.email;
    customer.gender = dto.gender;
    customer.birth_date = dto.birth_date;
    customer.religion = dto.religion;
    customer.phone_number = dto.phone_number;
    customer.occupation = dto.occupation;
    customer.emegrency_name = dto.emergency_name;
    customer.emergency_phone = dto.emergency_phone;
    customer.type = dto.type;
    customer.address = dto.address;
    customer.comment = dto.comment;
    customer.kost = kost;

    try {
      const data = await this.customerRepository.save(customer);
      return { status: HttpStatus.CREATED, data };
    } catch (error) {
      throw new HttpException({ error }, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  async update(
    dto: UpdateCustomerDTO,
    customerId: string,
    kostId: string,
    req: any,
  ) {
    const customer = await this.findByPuid(customerId);

    customer.name = dto.name;
    customer.gender = dto.gender;
    customer.birth_date = dto.birth_date;
    customer.religion = dto.religion;
    customer.phone_number = dto.phone_number;
    customer.occupation = dto.occupation;
    customer.emegrency_name = dto.emergency_name;
    customer.emergency_phone = dto.emergency_phone;
    customer.type = dto.type;
    customer.status = dto.status;
    customer.address = dto.address;
    customer.comment = dto.comment;

    if (kostId) {
      const kost = await this.kostService.findById(kostId, req);
      customer.kost = kost;
    }

    try {
      const data = await this.customerRepository.save(customer);
      return { status: HttpStatus.OK, data };
    } catch (error) {
      throw new HttpException({ error }, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  async delete(customerId: string) {
    const customer = await this.findByPuid(customerId);

    try {
      await this.customerRepository.remove(customer);
      return {
        status: HttpStatus.NO_CONTENT,
        message: 'Customer has been succesfully deleted',
      };
    } catch (error) {
      throw new HttpException({ error }, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  async isCustomerExist(email: string) {
    const data = await this.customerRepository.findOne({ email });

    if (data) {
      throw new HttpException(
        { error: 'Customer already exist' },
        HttpStatus.BAD_REQUEST,
      );
    }

    return data;
  }

  async findByPuid(customerId: string) {
    const data = await this.customerRepository.findOne({
      public_uid: customerId,
    });

    if (!data) {
      throw new HttpException(
        { error: 'No customer was found' },
        HttpStatus.BAD_REQUEST,
      );
    }

    return data;
  }

  async findMainByPuid(customerId: string) {
    const data = await this.customerRepository
      .createQueryBuilder('customer')
      .where('customer.public_uid = :customerId', { customerId })
      .andWhere('customer.type = :type', { type: CustomerType.MAIN })
      .getOne();

    if (!data) {
      throw new HttpException(
        { error: 'No customer was found' },
        HttpStatus.BAD_REQUEST,
      );
    }

    return data;
  }

  async findRoomateByPuid(customerId: string) {
    const data = await this.customerRepository
      .createQueryBuilder('customer')
      .where('customer.type = :type', { type: CustomerType.ROOMMATE })
      .andWhere('customer.public_uid = :customerId', { customerId })
      .andWhere('customer.status = :status', { status: CustomerStatus.ACTIVE })
      .getOne();

    if (!data) {
      throw new HttpException(
        { error: 'No customer with type Roommate was found' },
        HttpStatus.BAD_REQUEST,
      );
    }

    return data;
  }
}
