import { ApiModelProperty } from '@nestjs/swagger';
import { IsMobilePhone, IsEnum } from 'class-validator';
import {
  Occupation,
  Religion,
  Gender,
  CustomerStatus,
  CustomerType,
} from '../customer.entity';

export class UpdateCustomerDTO {
  @ApiModelProperty()
  readonly name: string;

  @ApiModelProperty({ enum: Gender })
  @IsEnum(Gender)
  readonly gender: Gender;

  @ApiModelProperty({ type: 'string', format: 'date-time' })
  readonly birth_date: Date;

  @ApiModelProperty({ enum: Occupation })
  @IsEnum(Occupation)
  readonly occupation: Occupation;

  @ApiModelProperty({ enum: CustomerType })
  @IsEnum(CustomerType)
  readonly type: CustomerType;

  @ApiModelProperty({ enum: CustomerStatus })
  @IsEnum(CustomerStatus)
  readonly status: CustomerStatus;

  @ApiModelProperty()
  @IsMobilePhone('id-ID')
  readonly phone_number: string;

  @ApiModelProperty({ enum: Religion })
  @IsEnum(Religion)
  readonly religion: Religion;

  @ApiModelProperty()
  readonly emergency_name: string;

  @ApiModelProperty()
  @IsMobilePhone('id-ID')
  readonly emergency_phone: string;

  @ApiModelProperty()
  readonly address: string;

  @ApiModelProperty()
  readonly comment: string;
}
