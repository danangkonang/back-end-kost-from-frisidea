import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsMobilePhone, IsEmail, IsEnum } from 'class-validator';
import { Occupation, Religion, Gender, CustomerType } from '../customer.entity';

export class CreateCustomerDTO {
  @ApiModelProperty()
  @IsNotEmpty()
  readonly name: string;

  @ApiModelProperty()
  @IsEmail()
  readonly email: string;

  @ApiModelProperty({ enum: Gender })
  @IsEnum(Gender)
  readonly gender: Gender;

  @ApiModelProperty({ type: 'string', format: 'date-time' })
  @IsNotEmpty()
  readonly birth_date: Date;

  @ApiModelProperty({ enum: Occupation })
  @IsEnum(Occupation)
  readonly occupation: Occupation;

  @ApiModelProperty({ enum: CustomerType })
  @IsEnum(CustomerType)
  readonly type: CustomerType;

  @ApiModelProperty()
  @IsMobilePhone('id-ID')
  readonly phone_number: string;

  @ApiModelProperty({ enum: Religion })
  @IsEnum(Religion)
  readonly religion: Religion;

  @ApiModelProperty()
  @IsNotEmpty()
  readonly emergency_name: string;

  @ApiModelProperty()
  @IsMobilePhone('id-ID')
  readonly emergency_phone: string;

  @ApiModelProperty()
  @IsNotEmpty()
  readonly address: string;

  @ApiModelProperty()
  @IsNotEmpty()
  readonly comment: string;
}
