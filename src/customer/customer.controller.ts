import {
  Controller,
  Get,
  Param,
  Body,
  Post,
  Req,
  UseGuards,
  Put,
  Query,
  Delete,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiImplicitQuery } from '@nestjs/swagger';

import { CreateCustomerDTO, UpdateCustomerDTO } from './dto';

import { CustomerService } from './customer.service';
import { IQuery } from '../common/interfaces';
import { StatusGuard } from '../common/guards/status.guard';
import { Status } from '../common/decorators';

@Controller()
export class CustomerController {
  constructor(private readonly customerService: CustomerService) {}

  @Get('/kosts/:kost_puid/customer-statuses')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'), StatusGuard)
  @Status('completed')
  async getCountStatus(@Param('kost_puid') kostId: string) {
    return this.customerService.getCountStatus(kostId);
  }

  @Get('/kosts/:kost_puid/customers')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'), StatusGuard)
  @Status('completed')
  async getAll(@Param('kost_puid') kostId: string, @Query() query: IQuery) {
    return this.customerService.getAll(kostId, query);
  }

  @Post('kosts/:kost_puid/customers')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'), StatusGuard)
  @Status('completed')
  async create(
    @Param('kost_puid') kostId: string,
    @Body() dto: CreateCustomerDTO,
    @Req() req: any,
  ) {
    return this.customerService.create(kostId, dto, req);
  }

  @Get('customers/:customer_puid/customer')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'), StatusGuard)
  @Status('completed')
  async getOne(@Param('customer_puid') customerId: string) {
    return this.customerService.getOne(customerId);
  }

  @Put('customers/:customer_puid/update')
  @ApiImplicitQuery({ name: 'kostId', required: false })
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'), StatusGuard)
  @Status('completed')
  async update(
    @Body() dto: UpdateCustomerDTO,
    @Param('customer_puid') customerId: string,
    @Query() kostId: string,
    @Req() req: any,
  ) {
    return this.customerService.update(dto, customerId, kostId, req);
  }

  @Delete('customers/:customer_puid/delete')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'), StatusGuard)
  @Status('completed')
  async delete(@Param('customer_puid') customerId: string) {
    return this.customerService.delete(customerId);
  }
}
