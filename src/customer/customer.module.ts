import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { KostModule } from '../kost/kost.module';
import { CustomerController } from './customer.controller';
import { CustomerService } from './customer.service';

import { Customer } from './customer.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Customer]), KostModule],
  controllers: [CustomerController],
  providers: [CustomerService],
  exports: [CustomerService],
})
export class CustomerModule {}
