import { NestFactory } from '@nestjs/core';
import { ValidationPipe } from '@nestjs/common';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import * as helmet from 'helmet';
// import * as csurf from 'csurf';
// import * as cookieParser from 'cookie-parser';

import { AppModule } from './app.module';
import { NotificationsInterceptor } from './common/interceptors/notifications.interceptor';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  const options = new DocumentBuilder()
    .setTitle('Swagger Docs')
    .setDescription('This a swagger docs')
    .setVersion('0.0.0')
    .addBearerAuth('Authorization', 'header')
    .build();

  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('api', app, document);

  app.enableCors();
  app.use(helmet());
  // app.use(cookieParser());
  // app.use(csurf({ cookie: true }));
  app.useGlobalPipes(new ValidationPipe());
  app.useGlobalInterceptors(new NotificationsInterceptor());

  await app.listen(5000);
}
bootstrap();
