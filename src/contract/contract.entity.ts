import {
  Entity,
  PrimaryGeneratedColumn,
  PrimaryColumn,
  Column,
  OneToOne,
  ManyToOne,
  CreateDateColumn,
  UpdateDateColumn,
  BeforeInsert,
  BeforeUpdate,
  OneToMany,
} from 'typeorm';
import * as uuid from 'uuid';
import * as moment from 'moment';

import { Room } from '../room/room.entity';
import { Customer } from '../customer/customer.entity';
import { RoomType } from '../room-type/room-type.entity';
import { HttpException, HttpStatus } from '@nestjs/common';
import { ContractHistory } from './contract-history.entity';
import { Reminder } from '../common/entities/schedule.entity';

export enum PaymentStatus {
  PAID = 'paid',
  UNPAID = 'unpaid',
}

export enum ContractStatus {
  ACTIVE_SOON = 'active_soon',
  ACTIVE = 'active',
  ENDED = 'ended',
  END_SOON = 'end_soon',
  CANCELED = 'canceled',
}

export enum PeriodType {
  DAILY = 'daily',
  WEEKLY = 'weekly',
  MONTHLY = 'monthly',
  YEARLY = 'yearly',
}

@Entity()
export class Contract {
  @PrimaryGeneratedColumn()
  id: number;

  @PrimaryColumn()
  public_uid: string;

  @Column()
  business_puid: string;

  @Column()
  kost_puid: string;

  @Column()
  customer_name: string;

  @Column({ type: 'enum', enum: ContractStatus })
  status: ContractStatus;

  @Column({ type: 'enum', enum: PaymentStatus })
  payment_status: PaymentStatus;

  @Column({ type: 'enum', enum: PeriodType })
  period_type: PeriodType;

  @Column()
  contract_start: Date;

  @Column()
  contract_end: Date;

  @Column()
  durations: number;

  @Column()
  cost_references: number;

  @OneToMany(() => AdditionalCostsContract, addCost => addCost.contract)
  additional_costs: AdditionalCostsContract[];

  @Column({ nullable: true })
  deposit: number;

  @OneToOne(() => Room, room => room.contract, { onDelete: 'SET NULL' })
  room: Room;

  @ManyToOne(() => RoomType, room_type => room_type.contracts, {
    onDelete: 'SET NULL',
  })
  room_type: RoomType;

  @OneToMany(() => Customer, customer => customer.contract, {
    onDelete: 'CASCADE',
  })
  customers: Customer[];

  @OneToMany(() => ContractHistory, history => history.contract)
  history: ContractHistory[];

  @OneToOne(() => Reminder, contractReminder => contractReminder.contract, {
    onDelete: 'CASCADE',
  })
  reminder: Reminder;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @BeforeInsert()
  generatePublicUid() {
    this.public_uid = uuid()
      .split('-')
      .pop();
  }

  @BeforeInsert()
  @BeforeUpdate()
  generateDuration() {
    let start = {
      day: moment(this.contract_start).dayOfYear(),
      week: moment(this.contract_start).week(),
      month: moment(this.contract_start).month() + 1,
      year: moment(this.contract_start).year(),
    };

    let end = {
      day: moment(this.contract_end).dayOfYear(),
      week: moment(this.contract_end).week(),
      month: moment(this.contract_end).month() + 1,
      year: moment(this.contract_end).year(),
    };

    if (this.period_type === PeriodType.DAILY) {
      if (end.year > start.year) {
        let days = moment([start.year, 11, 31]).dayOfYear();
        let newEnd = end.day + days;
        this.durations = newEnd - start.day;
      }
      this.durations = end.day - start.day;
    } else if (this.period_type === PeriodType.WEEKLY) {
      if (end.year > start.year) {
        let weeks = moment([start.year, 11, 31]).week();
        let newEnd = end.week + weeks;
        this.durations = newEnd - start.week;
      }
      this.durations = end.week - start.week;
    } else if (this.period_type === PeriodType.MONTHLY) {
      if (end.year > start.year) {
        let newEnd = end.month + 12;
        this.durations = newEnd - start.month;
      }
      this.durations = end.month - start.month;
    } else if (this.period_type === PeriodType.YEARLY) {
      this.durations = end.year - start.year;
    } else {
      throw new HttpException(
        { error: 'Fatal Error' },
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }
}

@Entity()
export class AdditionalCostsContract {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  price: number;

  @ManyToOne(() => Contract, contract => contract.additional_costs, {
    onDelete: 'CASCADE',
  })
  contract: Contract;
}
