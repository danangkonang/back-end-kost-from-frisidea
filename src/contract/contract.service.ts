import { Injectable, HttpStatus, HttpException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { validate } from 'class-validator';
import * as moment from 'moment';

import {
  CreateContractDTO,
  UpdateContractDTO,
  AdditionalCostDTO,
  UpdateCostDTO,
} from './dto';
import {
  Contract,
  ContractStatus,
  AdditionalCostsContract,
  PeriodType,
  PaymentStatus,
} from './contract.entity';
import { RoomStatus } from '../room/room.entity';
import { ContractHistory } from './contract-history.entity';
import { Reminder, ReminderType } from '../common/entities/schedule.entity';
import { BusinessInfo } from '../common/entities/business-info.entity';

import { RoomService } from '../room/room.service';
import { CustomerService } from '../customer/customer.service';

interface ReminderParams {
  periodType: PeriodType;
  groundTime: Date;
  businessInfo: BusinessInfo;
  reminder?: Reminder;
}

@Injectable()
export class ContractService {
  constructor(
    @InjectRepository(Contract)
    private readonly contractRepository: Repository<Contract>,
    @InjectRepository(ContractHistory)
    private readonly historyRepository: Repository<ContractHistory>,
    @InjectRepository(AdditionalCostsContract)
    private readonly addCostRepository: Repository<AdditionalCostsContract>,
    @InjectRepository(Reminder)
    private readonly reminderRepository: Repository<Reminder>,
    @InjectRepository(BusinessInfo)
    private readonly businessInfoRepository: Repository<BusinessInfo>,
    private readonly roomService: RoomService,
    private readonly customerService: CustomerService,
  ) {}

  async getAll(kostId: string) {
    const data = await this.contractRepository
      .createQueryBuilder('contract')
      .leftJoinAndSelect('contract.additional_costs', 'additional_costs')
      .leftJoinAndSelect('contract.history', 'history')
      .leftJoin('contract.room', 'room')
      .leftJoin('contract.customers', 'customers')
      .where('contract.kost_puid = :kostId', { kostId })
      .select()
      .addSelect(['room.public_uid', 'room.number'])
      .addSelect(['customers.public_uid', 'customers.name', 'customers.email'])
      .getMany();
    return { status: HttpStatus.OK, data };
  }

  async getCountStatus(kostId: string) {
    let data = {
      active: 0,
      active_soon: 0,
      end_soon: 0,
      ended: 0,
      canceled: 0,
    };

    const contracts = await this.contractRepository
      .createQueryBuilder('contract')
      .where('contract.kost_puid = :kostId', { kostId })
      .getMany();

    if (!data) {
      throw new HttpException(
        { error: 'No Contracts was found' },
        HttpStatus.NOT_FOUND,
      );
    }

    for (let i = 0; i < contracts.length; i++) {
      let el = contracts[i];
      if (el.status === ContractStatus.ACTIVE) {
        data.active += 1;
      } else if (el.status === ContractStatus.ACTIVE_SOON) {
        data.active_soon += 1;
      } else if (el.status === ContractStatus.END_SOON) {
        data.end_soon += 1;
      } else if (el.status === ContractStatus.ENDED) {
        data.ended += 1;
      } else {
        data.canceled += 1;
      }
    }

    return { status: HttpStatus.OK, data };
  }

  async create(
    dto: CreateContractDTO,
    roomId: string,
    businessInfo: BusinessInfo,
  ) {
    const room = await this.roomService.findByPuid(roomId);

    const mainCustomer = await this.customerService.findMainByPuid(
      dto.customerId,
    );

    if (dto.contractStart === dto.contractEnd) {
      throw new HttpException(
        { error: 'Contract start and contract end cannot be the same' },
        HttpStatus.BAD_REQUEST,
      );
    }

    const contract = new Contract();
    const newAdditionalCost = new AdditionalCostsContract();

    const costRef = room.room_type.cost_references.find(
      item => item.period_type === dto.periodType,
    );

    if (!costRef) {
      throw new HttpException(
        { error: 'Cost Reference not found' },
        HttpStatus.BAD_REQUEST,
      );
    }

    const roommates = dto.roommateIds
      ? dto.roommateIds.map(item => {
          return this.customerService.findRoomateByPuid(item);
        })
      : [];
    const addCost = dto.additionalCost
      ? dto.additionalCost.map(item => {
          newAdditionalCost.name = item.name;
          newAdditionalCost.price = item.price;
          return this.addCostRepository.save(newAdditionalCost);
        })
      : [];

    const customers = [mainCustomer, ...(await Promise.all(roommates))];

    if (customers.length > room.room_type.capacity) {
      throw new HttpException(
        { error: 'Customer exceed the limit capacity' },
        HttpStatus.BAD_REQUEST,
      );
    }

    contract.kost_puid = room.kost_puid;
    contract.customer_name = mainCustomer.name;
    contract.contract_start = dto.contractStart;
    contract.contract_end = dto.contractEnd;
    contract.period_type = dto.periodType;
    contract.status = dto.contractStatus;
    contract.payment_status = PaymentStatus.UNPAID;
    contract.room_type = room.room_type;
    contract.cost_references = dto.costReferences;
    contract.additional_costs = await Promise.all(addCost);
    contract.deposit = costRef.deposit;
    contract.room = room;
    contract.customers = customers;
    contract.business_puid = businessInfo.public_uid;

    // FOR GENERATING REMINDER

    const reminderParams: ReminderParams = {
      periodType: dto.periodType,
      groundTime: dto.contractStart,
      businessInfo: businessInfo,
    };
    contract.reminder = await this.generateDeadlineReminder(reminderParams);

    // END GENERATING REMINDER

    try {
      await validate(contract);
      const data = await this.contractRepository.save(contract);
      await this.roomService.changeRoomStatus(room, RoomStatus.NOT_AVAILABLE);
      return { status: HttpStatus.CREATED, data };
    } catch (error) {
      throw new HttpException({ error }, HttpStatus.BAD_REQUEST);
    }
  }

  async update(
    contractId: string,
    dto: UpdateContractDTO,
    businessInfo: BusinessInfo,
  ) {
    const contract = await this.findByPuid(contractId);
    const { reminder } = contract;
    const roommates = dto.roommateIds.map(item => {
      return this.customerService.findRoomateByPuid(item);
    });

    const mainCustomer = await this.customerService.findMainByPuid(
      dto.customerId,
    );

    const customers = [mainCustomer, ...(await Promise.all(roommates))];

    if (customers.length > contract.room_type.capacity) {
      throw new HttpException(
        { error: 'Customers exceed the limit capacity' },
        HttpStatus.BAD_REQUEST,
      );
    }

    contract.period_type = dto.periodType;
    contract.customers = customers;
    contract.contract_start = dto.contractStart;
    contract.contract_end = dto.contractEnd;

    if (contract.payment_status === PaymentStatus.PAID) {
      await this.updateContractReminder({
        periodType: dto.periodType,
        groundTime: dto.contractEnd,
        businessInfo: businessInfo,
        reminder: reminder,
      });
    } else {
      await this.updateDeadlineReminder({
        periodType: dto.periodType,
        groundTime: dto.contractStart,
        businessInfo: businessInfo,
        reminder: reminder,
      });
    }

    try {
      const data = await this.contractRepository.save(contract);
      return { status: HttpStatus.OK, data };
    } catch (error) {
      throw new HttpException({ error }, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  async updateCost(contractId: string, dto: UpdateCostDTO, req: any) {
    const contract = await this.findByPuid(contractId);

    if (dto.costReference) {
      const history = new ContractHistory();
      history.cost_name = 'Cost Reference';
      history.admin_name = req.user.full_name;
      history.cost_before = contract.cost_references;
      history.cost_after = dto.costReference;
      history.contract = contract;
      history.kost_puid = contract.kost_puid;
      contract.cost_references = dto.costReference;
      try {
        await this.historyRepository.save(history);
      } catch (error) {
        throw new HttpException({ error }, HttpStatus.INTERNAL_SERVER_ERROR);
      }
    }

    if (dto.deposit) {
      const history = new ContractHistory();
      history.cost_name = 'Deposit';
      history.admin_name = req.user.full_name;
      history.cost_before = contract.deposit;
      history.cost_after = dto.deposit;
      history.kost_puid = contract.kost_puid;
      history.contract = contract;
      contract.deposit = dto.deposit;
      try {
        await this.historyRepository.save(history);
      } catch (error) {
        throw new HttpException({ error }, HttpStatus.INTERNAL_SERVER_ERROR);
      }
    }

    await this.contractRepository.save(contract);
    return { status: HttpStatus.OK, data: 'Updated' };
  }

  async addNewAdditionalCosts(contractId: string, dto: AdditionalCostDTO) {
    const contract = await this.findByPuid(contractId);

    const newAdditionalCost = new AdditionalCostsContract();

    newAdditionalCost.name = dto.name;
    newAdditionalCost.price = dto.price;
    newAdditionalCost.contract = contract;

    try {
      const data = await this.addCostRepository.save(newAdditionalCost);
      return { status: HttpStatus.CREATED, data };
    } catch (error) {
      throw new HttpException({ error }, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  async deleteAddCost(addCostId: number) {
    try {
      const data = await this.addCostRepository.findOne(addCostId);
      await this.addCostRepository.remove(data);
      return { status: HttpStatus.NO_CONTENT, data: 'Deleted' };
    } catch (error) {
      throw new HttpException({ error }, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  async delete(contractId: string) {
    const contract = await this.findByPuid(contractId);

    try {
      await this.contractRepository.remove(contract);
      await this.roomService.changeRoomStatus(
        contract.room,
        RoomStatus.AVAILABLE,
      );
      return {
        status: HttpStatus.NO_CONTENT,
        data: 'Contract has been successfully deleted',
      };
    } catch (error) {
      throw new HttpException({ error }, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  async findByPuid(contractId: string) {
    const data = await this.contractRepository
      .createQueryBuilder('contract')
      .leftJoinAndSelect('contract.additional_costs', 'additional_costs')
      .leftJoinAndSelect('contract.customers', 'customers')
      .leftJoinAndSelect('contract.room', 'room')
      .leftJoinAndSelect('contract.room_type', 'room_type')
      .leftJoinAndSelect('contract.reminder', 'reminder')
      .where('contract.public_uid = :contractId', { contractId })
      .getOne();

    if (!data) {
      throw new HttpException(
        { error: 'No Contract was found' },
        HttpStatus.NOT_FOUND,
      );
    }

    return data;
  }

  async paysContract(contract: Contract) {
    contract.payment_status = PaymentStatus.PAID;
    const businessInfo = await this.businessInfoRepository.findOne(
      contract.business_puid,
    );

    const reminderParams: ReminderParams = {
      periodType: contract.period_type,
      groundTime: contract.contract_end,
      businessInfo: businessInfo,
    };

    try {
      await this.generateContractReminder(reminderParams);
      return this.contractRepository.save(contract);
    } catch (error) {
      throw new HttpException({ error }, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  getReminderDay(endDate: Date, conf: number) {
    const parsed = Date.parse(`${endDate}`);
    const date = parsed - conf * 24 * 60 * 60 * 1000;
    return {
      day: moment(date).format('DDMMYY'),
      hour: moment(endDate).format('HH'),
    };
  }

  getReminderHour(endDate: Date, conf: number) {
    const parsed = Date.parse(`${endDate}`);
    const hour = parsed - conf * 60 * 60 * 1000;
    return {
      day: moment(endDate).format('DDMMYY'),
      hour: moment(hour).format('HH'),
    };
  }

  async generateContractReminder(params: ReminderParams) {
    const newContractReminder = new Reminder();

    if (params.periodType === PeriodType.DAILY) {
      const trigger = this.getReminderHour(
        params.groundTime,
        params.businessInfo.hourly_contract_reminder,
      );

      newContractReminder.trigger_date = trigger.day;
      newContractReminder.trigger_hours = trigger.hour;
      newContractReminder.period_type = params.periodType;
      newContractReminder.type = ReminderType.CONTRACT;

      return this.reminderRepository.save(newContractReminder);
    } else {
      const trigger = this.getReminderDay(
        params.groundTime,
        params.businessInfo.daily_contract_reminder,
      );

      newContractReminder.trigger_date = trigger.day;
      newContractReminder.trigger_hours = trigger.hour;
      newContractReminder.period_type = params.periodType;
      newContractReminder.type = ReminderType.CONTRACT;

      return this.reminderRepository.save(newContractReminder);
    }
  }

  async generateDeadlineReminder(params: ReminderParams) {
    const newDeadlineReminder = new Reminder();

    if (params.periodType === PeriodType.DAILY) {
      const trigger = this.getReminderHour(
        params.groundTime,
        params.businessInfo.hourly_deadline_reminder,
      );

      newDeadlineReminder.trigger_date = trigger.day;
      newDeadlineReminder.trigger_hours = trigger.hour;
      newDeadlineReminder.period_type = params.periodType;
      newDeadlineReminder.type = ReminderType.DEADLINE;

      return this.reminderRepository.save(newDeadlineReminder);
    } else {
      const trigger = this.getReminderDay(
        params.groundTime,
        params.businessInfo.daily_deadline_reminder,
      );

      newDeadlineReminder.trigger_date = trigger.day;
      newDeadlineReminder.trigger_hours = trigger.hour;
      newDeadlineReminder.period_type = params.periodType;
      newDeadlineReminder.type = ReminderType.DEADLINE;

      return this.reminderRepository.save(newDeadlineReminder);
    }
  }

  async updateContractReminder(params: ReminderParams) {
    if (params.periodType === PeriodType.DAILY) {
      const trigger = this.getReminderHour(
        params.groundTime,
        params.businessInfo.hourly_contract_reminder,
      );

      params.reminder.trigger_date = trigger.day;
      params.reminder.trigger_hours = trigger.hour;

      await this.reminderRepository.save(params.reminder);
    } else {
      const trigger = this.getReminderDay(
        params.groundTime,
        params.businessInfo.daily_contract_reminder,
      );

      params.reminder.trigger_date = trigger.day;
      params.reminder.trigger_hours = trigger.hour;

      await this.reminderRepository.save(params.reminder);
    }
  }

  async updateDeadlineReminder(params: ReminderParams) {
    if (params.periodType === PeriodType.DAILY) {
      const trigger = this.getReminderHour(
        params.groundTime,
        params.businessInfo.hourly_deadline_reminder,
      );

      params.reminder.trigger_date = trigger.day;
      params.reminder.trigger_hours = trigger.hour;

      await this.reminderRepository.save(params.reminder);
    } else {
      const trigger = this.getReminderDay(
        params.groundTime,
        params.businessInfo.daily_deadline_reminder,
      );

      params.reminder.trigger_date = trigger.day;
      params.reminder.trigger_hours = trigger.hour;

      await this.reminderRepository.save(params.reminder);
    }
  }
}
