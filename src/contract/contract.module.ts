import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { ContractController } from './contract.controller';
import { ContractService } from './contract.service';
import { Contract, AdditionalCostsContract } from './contract.entity';
import { ContractHistory } from './contract-history.entity';

import { RoomModule } from '../room/room.module';
import { CustomerModule } from '../customer/customer.module';
import { Reminder } from '../common/entities/schedule.entity';
import { BusinessInfo } from '../common/entities/business-info.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      Contract,
      ContractHistory,
      AdditionalCostsContract,
      Reminder,
      BusinessInfo,
    ]),
    RoomModule,
    CustomerModule,
  ],
  controllers: [ContractController],
  providers: [ContractService],
  exports: [ContractService],
})
export class ContractModule {}
