import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  BeforeInsert,
} from 'typeorm';
import { Contract } from './contract.entity';

@Entity()
export class ContractHistory {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  admin_name: string;

  @Column()
  kost_puid: string;

  @Column()
  date: Date;

  @Column()
  cost_name: string;

  @Column()
  cost_before: number;

  @Column()
  cost_after: number;

  @ManyToOne(() => Contract, contract => contract.history)
  contract: Contract;

  @BeforeInsert()
  generateDate() {
    this.date = new Date();
  }
}
