import {
  Controller,
  Get,
  UseGuards,
  Param,
  Post,
  Body,
  Delete,
  Put,
  Req,
} from '@nestjs/common';
import { ApiBearerAuth } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';

import { ContractService } from './contract.service';
import {
  CreateContractDTO,
  UpdateContractDTO,
  AdditionalCostDTO,
  UpdateCostDTO,
} from './dto';
import { StatusGuard } from '../common/guards/status.guard';
import { Status } from '../common/decorators';

@Controller()
export class ContractController {
  constructor(private readonly contractService: ContractService) {}

  @Get('kosts/:kost_puid/contracts')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'), StatusGuard)
  @Status('completed')
  async getAll(@Param('kost_puid') kostId: string) {
    return this.contractService.getAll(kostId);
  }

  @Get('kosts/:kost_puid/contracts-status')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'), StatusGuard)
  @Status('completed')
  async getCountStatus(@Param('kost_puid') kostId: string) {
    return this.contractService.getCountStatus(kostId);
  }

  @Post('room/:room_puid/contracts')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'), StatusGuard)
  @Status('completed')
  async create(
    @Param('room_puid') roomId: string,
    @Body() dto: CreateContractDTO,
    @Req() req: Express.Request,
  ) {
    const { business_info } = req.user;
    return this.contractService.create(dto, roomId, business_info);
  }

  @Put('contracts/:contract_puid/update')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'), StatusGuard)
  @Status('completed')
  async update(
    @Param('contract_puid') contractId: string,
    @Body() dto: UpdateContractDTO,
    @Req() req: Express.Request,
  ) {
    const { business_info } = req.user;
    return this.contractService.update(contractId, dto, business_info);
  }

  @Put('contracts/:contract_puid/update-cost')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'), StatusGuard)
  @Status('completed')
  async updateCost(
    @Param('contract_puid') contractId: string,
    @Body() dto: UpdateCostDTO,
    @Req() req: any,
  ) {
    return this.contractService.updateCost(contractId, dto, req);
  }

  @Delete('contracts/:contract_puid/delete')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'), StatusGuard)
  @Status('completed')
  async delete(@Param('contract_puid') contractId: string) {
    return this.contractService.delete(contractId);
  }

  @Post('contracts/:contract_puid/additional-cost')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'), StatusGuard)
  @Status('completed')
  async createAdditionalCost(
    @Param('contract_puid') contractId: string,
    @Body() dto: AdditionalCostDTO,
  ) {
    return this.contractService.addNewAdditionalCosts(contractId, dto);
  }

  @Delete('contracts-addCost/:addCostId/delete')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'), StatusGuard)
  @Status('completed')
  async deleteAdditionalCost(@Param('addCostId') addCostId: number) {
    this.contractService.deleteAddCost(addCostId);
  }
}
