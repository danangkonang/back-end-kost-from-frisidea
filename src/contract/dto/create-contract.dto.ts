import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsEnum, IsString, IsNumber } from 'class-validator';
import { PaymentStatus, ContractStatus, PeriodType } from '../contract.entity';

export class AdditionalCostDTO {
  @ApiModelProperty()
  @IsString()
  name: string;

  @ApiModelProperty()
  @IsNumber()
  price: number;
}
export class CreateContractDTO {
  @ApiModelProperty()
  @IsNotEmpty()
  readonly customerId: string;

  @ApiModelProperty({ enum: ContractStatus })
  @IsEnum(ContractStatus)
  readonly contractStatus: ContractStatus;

  @ApiModelProperty({ enum: PeriodType })
  @IsEnum(PeriodType)
  readonly periodType: PeriodType;

  @ApiModelProperty()
  readonly roommateIds: string[];

  @ApiModelProperty()
  @IsNotEmpty()
  readonly costReferences: number;

  @ApiModelProperty({ type: AdditionalCostDTO, isArray: true })
  readonly additionalCost: AdditionalCostDTO[];

  @ApiModelProperty({ type: 'string', format: 'date-time' })
  @IsNotEmpty()
  readonly contractStart: Date;

  @ApiModelProperty({ type: 'string', format: 'date-time' })
  @IsNotEmpty()
  readonly contractEnd: Date;
}
