import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsEnum, IsString, IsNumber } from 'class-validator';
import { PaymentStatus, ContractStatus, PeriodType } from '../contract.entity';

export class UpdateContractDTO {
  @ApiModelProperty()
  @IsNotEmpty()
  readonly customerId: string;

  @ApiModelProperty({ enum: ContractStatus })
  @IsEnum(ContractStatus)
  readonly contractStatus: ContractStatus;

  @ApiModelProperty({ enum: PeriodType })
  @IsEnum(PeriodType)
  readonly periodType: PeriodType;

  @ApiModelProperty()
  readonly roommateIds: string[];

  @ApiModelProperty({ type: 'string', format: 'date-time' })
  @IsNotEmpty()
  readonly contractStart: Date;

  @ApiModelProperty({ type: 'string', format: 'date-time' })
  @IsNotEmpty()
  readonly contractEnd: Date;
}
