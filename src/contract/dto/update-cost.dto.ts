import { ApiModelProperty } from '@nestjs/swagger';

export class UpdateCostDTO {
  @ApiModelProperty()
  readonly costReference: number;

  @ApiModelProperty()
  readonly deposit: number;
}
