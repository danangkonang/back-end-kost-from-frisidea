import { getRepository } from 'typeorm';

import { User, Gender, Status } from './user/user.entity';
import { Kost, Type, VisitHours, PetsPolicy } from './kost/kost.entity';
import { AuthLog } from './auth/auth-log.entity';
import {
  RoomType,
  CostReference,
  PeriodType,
  Facility,
} from './room-type/room-type.entity';
import { Room, RoomStatus } from './room/room.entity';
import {
  Customer,
  CustomerStatus,
  CustomerType,
} from './customer/customer.entity';
import { Religion, Occupation } from './customer/customer.entity';
import { BusinessInfo } from './common/entities/business-info.entity';

export const seed = async () => {
  const user = getRepository(User);
  const kost = getRepository(Kost);
  const authLog = getRepository(AuthLog);
  const roomType = getRepository(RoomType);
  const room = getRepository(Room);
  const customer = getRepository(Customer);
  const businessInfo = getRepository(BusinessInfo);
  const costReference = getRepository(CostReference);
  const facility = getRepository(Facility);

  // USER SEED

  const newUser = new User();
  const newLog = new AuthLog();

  newUser.email = 'hadi@example.com';
  newUser.full_name = 'Muhammad Hadi';
  newUser.password = 'Password1!';
  newUser.phone_number = '085921512171';
  newUser.gender = Gender.MALE;
  newUser.status = Status.COMPLETED;
  await newUser.hashPassword();
  const createdUser = await user.save(newUser);

  newLog.status = 'registered';
  newLog.user = createdUser;

  await authLog.save(newLog);

  // KOST SEED

  const newKost = new Kost();

  newKost.name = 'Skynet Kost';
  newKost.address = 'Sunset Bouleavrd';
  newKost.type = Type.MIX;
  newKost.visit_hours = VisitHours.FREE;
  newKost.pets_policy = PetsPolicy.ALLOW;
  newKost.province = 'California';
  newKost.city = 'Hollywood';
  newKost.district = 'North Hollywoord';
  newKost.sub_district = 'Sunset';
  newKost.postal_code = '90000';
  newKost.users = [createdUser];

  const savedKost = await kost.save(newKost);

  // BUSINESS INFO

  const newInfo = new BusinessInfo();

  newInfo.employees = [createdUser];
  newInfo.kosts = [savedKost];
  newInfo.sub_domain = 'skynet-kost';

  await businessInfo.save(newInfo);

  // KOST TYPE SEED

  const newType = new RoomType();
  const costWeekly = new CostReference();
  const costMonthly = new CostReference();
  const costDaily = new CostReference();

  const facilities = ['AC', 'KASUR', 'KAMAR MANDI'].map(item => {
    const newFacility = new Facility();
    newFacility.name = item;
    return facility.save(newFacility);
  });

  costDaily.price = 1000;
  costDaily.deposit = 500;
  costDaily.period_type = PeriodType.DAILY;

  costWeekly.price = 100000;
  costWeekly.deposit = 20000;
  costWeekly.period_type = PeriodType.WEEKLY;

  costMonthly.price = 500000;
  costMonthly.deposit = 30000;
  costMonthly.period_type = PeriodType.MONTHLY;

  newType.name = 'SINGLE';
  newType.facilities = await Promise.all(facilities);
  newType.capacity = 2;
  newType.cost_references = await Promise.all([
    costReference.save(costWeekly),
    costReference.save(costMonthly),
    costReference.save(costDaily),
  ]);
  newType.kost = savedKost;

  const savedType = await roomType.save(newType);

  // ROOMS SEED

  const rooms = [
    {
      number: 1,
      status: RoomStatus.AVAILABLE,
    },
    {
      number: 2,
      status: RoomStatus.AVAILABLE,
    },
    {
      number: 3,
      status: RoomStatus.AVAILABLE,
    },
    {
      number: 4,
      status: RoomStatus.MAINTENANCE,
    },
    {
      number: 5,
      status: RoomStatus.MAINTENANCE,
    },
  ];

  const createdRooms = rooms.map(item => {
    const newRoom = new Room();

    newRoom.number = item.number;
    newRoom.status = item.status;
    newRoom.room_type = savedType;
    newRoom.kost_puid = savedKost.public_uid;

    return room.save(newRoom);
  });

  await Promise.all(createdRooms);

  // CUSTOMER SEED

  // const = new Customer();
  const customerObj = [
    {
      name: 'John Connor',
      email: 'john_connor@skynet.com',
      gender: Gender.MALE,
      birth_date: new Date(),
      religion: Religion.CHRISTIAN,
      phone_number: '085711112222',
      occupation: Occupation.WORKER,
      emegrency_name: 'Sarah Connor',
      emergency_phone: '08542321822',
      type: CustomerType.MAIN,
      address: 'Sky Net',
      comment: 'The Choosen One',
    },
    {
      name: 'Sarah Connor',
      email: 'sarah_connor@skynet.com',
      gender: Gender.FEMALE,
      birth_date: new Date(),
      religion: Religion.CHRISTIAN,
      phone_number: '085711113333',
      occupation: Occupation.WORKER,
      emegrency_name: 'Kyle Reese',
      emergency_phone: '08542321855',
      type: CustomerType.MAIN,
      address: 'Sky Net',
      comment: 'The Wife',
    },
    {
      name: 'Kyle Reese',
      email: 'kyle_reese@skynet.com',
      gender: Gender.MALE,
      birth_date: new Date(),
      religion: Religion.CHRISTIAN,
      phone_number: '085711114444',
      occupation: Occupation.STUDENT,
      emegrency_name: 'Sarah Connor',
      emergency_phone: '085711113333',
      type: CustomerType.MAIN,
      address: 'Sky Net',
      comment: 'The Husband',
    },
    {
      name: 'T-1000',
      email: 't_1000@skynet.com',
      gender: Gender.FEMALE,
      birth_date: new Date(),
      religion: Religion.OTHER,
      phone_number: '089999911',
      occupation: Occupation.STUDENT,
      emegrency_name: 'Sky Net',
      emergency_phone: '089991212',
      status: CustomerStatus.ACTIVE,
      type: CustomerType.ROOMMATE,
      address: 'Sky Net',
      comment: 'T-1000',
    },
    {
      name: 'T-X',
      email: 't_x@skynet.com',
      gender: Gender.FEMALE,
      birth_date: new Date(),
      religion: Religion.OTHER,
      phone_number: '089999922',
      occupation: Occupation.WORKER,
      emegrency_name: 'Sky Net',
      emergency_phone: '089991212',
      status: CustomerStatus.BLOCKED,
      type: CustomerType.ROOMMATE,
      address: 'Sky Net',
      comment: 'T-X',
    },
  ];

  const customers = customerObj.map(item => {
    const newCustomer = new Customer();

    newCustomer.name = item.name;
    newCustomer.email = item.email;
    newCustomer.gender = item.gender;
    newCustomer.birth_date = item.birth_date;
    newCustomer.religion = item.religion;
    newCustomer.phone_number = item.phone_number;
    newCustomer.occupation = item.occupation;
    newCustomer.emegrency_name = item.emegrency_name;
    newCustomer.emergency_phone = item.emergency_phone;
    newCustomer.type = item.type;
    newCustomer.status = item.status;
    newCustomer.address = item.address;
    newCustomer.comment = item.comment;
    newCustomer.kost = savedKost;

    return customer.save(newCustomer);
  });

  await Promise.all(customers);
};
