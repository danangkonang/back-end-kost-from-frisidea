import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { RoomController } from './room.controller';
import { RoomService } from './room.service';

import { Room } from '../room/room.entity';
import { RoomType } from '../room-type/room-type.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Room, RoomType])],
  controllers: [RoomController],
  providers: [RoomService],
  exports: [RoomService],
})
export class RoomModule {}
