import {
  Entity,
  PrimaryGeneratedColumn,
  PrimaryColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  OneToMany,
  ManyToOne,
  BeforeInsert,
  OneToOne,
  JoinColumn,
} from 'typeorm';
import { RoomType } from '../room-type/room-type.entity';
import * as uuid from 'uuid';
import { Contract } from '../contract/contract.entity';
import { Queue } from '../queue/queue.entity';

export enum RoomStatus {
  PARTIAL_AVAILABLE = 'partial_available',
  AVAILABLE = 'available',
  NOT_AVAILABLE = 'not_available',
  MAINTENANCE = 'maintenance',
}

@Entity()
export class Room {
  @PrimaryGeneratedColumn()
  id: number;

  @PrimaryColumn()
  public_uid: string;

  @Column()
  kost_puid: string;

  @Column()
  number: number;

  @Column({ type: 'enum', enum: RoomStatus, default: RoomStatus.AVAILABLE })
  status: RoomStatus;

  @ManyToOne(() => RoomType, room_type => room_type.rooms, {
    onDelete: 'CASCADE',
  })
  room_type: RoomType;

  @OneToOne(() => Contract, contract => contract.room, { onDelete: 'SET NULL' })
  @JoinColumn()
  contract: Contract;

  @OneToMany(() => Queue, queue => queue.room, { onDelete: 'SET NULL' })
  queues: Queue[];

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updataedAt: Date;

  @BeforeInsert()
  generatedPublicUid() {
    this.public_uid = uuid()
      .split('-')
      .pop();
  }
}
