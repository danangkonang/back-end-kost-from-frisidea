import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { validate } from 'class-validator';

import { Room, RoomStatus } from './room.entity';
import { RoomType } from '../room-type/room-type.entity';
import { CreateRoomDTO, UpdateRoomDTO } from './dto';
import { IQuery } from '../common/interfaces';

@Injectable()
export class RoomService {
  constructor(
    @InjectRepository(Room) private readonly roomRepository: Repository<Room>,
    @InjectRepository(RoomType)
    private readonly roomTypeRepository: Repository<RoomType>,
  ) {}

  async getAll(query: IQuery, roomTypeId: string) {
    const roomType = await this.roomTypeRepository.findOne({
      public_uid: roomTypeId,
    });

    if (!roomType) {
      throw new HttpException(
        { error: 'No RoomType was found' },
        HttpStatus.NOT_FOUND,
      );
    }

    const take = query.take || 10;
    const skip = query.skip || 0;

    const data = await this.roomRepository
      .createQueryBuilder('room')
      .leftJoin('room.room_type', 'room_type')
      .leftJoin('room.contract', 'contract')
      .where('room_type.public_uid = :roomTypeId', { roomTypeId })
      .select()
      .orderBy('room.createdAt', query.order)
      .skip(skip)
      .take(take)
      .getMany();

    return { status: HttpStatus.OK, data };
  }

  async create(dto: CreateRoomDTO, roomTypeId: string) {
    const roomType = await this.roomTypeRepository
      .createQueryBuilder('room_type')
      .leftJoinAndSelect('room_type.kost', 'kost')
      .where('room_type.public_uid = :roomTypeId', { roomTypeId })
      .getOne();

    if (!roomType) {
      throw new HttpException(
        { error: 'Room Type was not found' },
        HttpStatus.NOT_FOUND,
      );
    }

    const room = new Room();

    room.kost_puid = roomType.kost.public_uid;
    room.number = dto.number;
    room.status = dto.status;
    room.room_type = roomType;

    try {
      await validate(room);
      const data = await this.roomRepository.save(room);
      return { status: HttpStatus.CREATED, data };
    } catch (error) {
      throw new HttpException({ error }, HttpStatus.BAD_REQUEST);
    }
  }

  async getOne(roomId: string) {
    const data = await this.roomRepository
      .createQueryBuilder('room')
      .where('room.public_uid = :roomId', { roomId })
      .getOne();

    if (!data) {
      throw new HttpException(
        { error: 'No Room was found' },
        HttpStatus.NOT_FOUND,
      );
    }
    return { status: HttpStatus.OK, data };
  }

  async update(dto: UpdateRoomDTO, roomId: string) {
    const room = await this.findByPuid(roomId);

    room.number = dto.number;
    room.status = dto.status;

    try {
      const data = await this.roomRepository.save(room);
      return { status: HttpStatus.OK, data };
    } catch (error) {
      throw new HttpException({ error }, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  async delete(roomId: string) {
    const data = await this.findByPuid(roomId);

    try {
      await this.roomRepository.remove(data);
      return {
        status: HttpStatus.NO_CONTENT,
        data: 'Room has been successfully deleted',
      };
    } catch (error) {
      throw new HttpException({ error }, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  async findByPuid(roomId: string) {
    const data = await this.roomRepository
      .createQueryBuilder('room')
      .leftJoinAndSelect('room.room_type', 'room_type')
      .leftJoinAndSelect('room_type.cost_references', 'cost_references')
      .leftJoinAndSelect('room.queues', 'queues')
      .where('room.public_uid = :roomId', { roomId })
      .getOne();

    if (!data) {
      throw new HttpException(
        { error: 'No Room was found' },
        HttpStatus.NOT_FOUND,
      );
    }

    return data;
  }

  async changeRoomStatus(room: Room, status: RoomStatus) {
    try {
      room.status = status;
      return this.roomRepository.save(room);
    } catch (error) {
      throw new HttpException({ error }, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
