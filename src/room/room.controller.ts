import {
  Controller,
  Post,
  Body,
  Param,
  UseGuards,
  Get,
  Put,
  Delete,
  UseInterceptors,
  Query,
} from '@nestjs/common';

import { RoomService } from './room.service';
import { CreateRoomDTO, UpdateRoomDTO } from './dto';
import { ApiBearerAuth, ApiImplicitQuery } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';

import { RoomCheckInterceptor } from '../common/interceptors';
import { IQuery } from '../common/interfaces';
import { StatusGuard } from '../common/guards';
import { Status } from '../common/decorators';

@Controller()
export class RoomController {
  constructor(private readonly roomService: RoomService) {}

  @Get('room-types/:types_puid/rooms')
  @ApiImplicitQuery({ name: 'skip', required: false })
  @ApiImplicitQuery({ name: 'take', required: false })
  @ApiImplicitQuery({ name: 'order', required: false })
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'), StatusGuard)
  @Status('completed')
  async getAll(
    @Query() query: IQuery,
    @Param('types_puid') roomTypeId: string,
  ) {
    return this.roomService.getAll(query, roomTypeId);
  }

  @Post('room-types/:types_puid/rooms')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'), StatusGuard)
  @Status('completed')
  async create(
    @Param('types_puid') roomTypeId: string,
    @Body() dto: CreateRoomDTO,
  ) {
    return this.roomService.create(dto, roomTypeId);
  }

  @Get('rooms/:room_puid')
  @UseInterceptors(new RoomCheckInterceptor())
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'), StatusGuard)
  @Status('completed')
  async getOne(@Param('room_puid') roomId: string) {
    return this.roomService.getOne(roomId);
  }

  @Put('rooms/:room_puid/update')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'), StatusGuard)
  @Status('completed')
  async update(@Param('room_puid') roomId: string, @Body() dto: UpdateRoomDTO) {
    return this.roomService.update(dto, roomId);
  }

  @Delete('rooms/:room_puid/delete')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'), StatusGuard)
  @Status('completed')
  async delete(@Param('room_puid') roomId: string) {
    return this.roomService.delete(roomId);
  }
}
