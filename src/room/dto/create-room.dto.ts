import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsEnum } from 'class-validator';
import { RoomStatus } from '../room.entity';

export class CreateRoomDTO {
  @ApiModelProperty()
  @IsNotEmpty()
  number: number;

  @ApiModelProperty({ enum: RoomStatus })
  @IsEnum(RoomStatus)
  status: RoomStatus;
}
