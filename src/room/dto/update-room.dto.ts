import { ApiModelProperty } from '@nestjs/swagger';
import { RoomStatus } from '../room.entity';

export class UpdateRoomDTO {
  @ApiModelProperty()
  number: number;

  @ApiModelProperty()
  status: RoomStatus;
}
