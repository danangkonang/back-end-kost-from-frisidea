import { IsEnum } from 'class-validator';

import { Role } from '../user.entity';
import { ApiModelProperty } from '@nestjs/swagger';

export class UpdateEmployeeDTO {
  @ApiModelProperty()
  @IsEnum(Role)
  role: Role;

  @ApiModelProperty()
  readonly kostIds: string[];
}
