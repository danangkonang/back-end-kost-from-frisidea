import { ApiModelProperty } from '@nestjs/swagger';

import { Gender } from '../user.entity';
export class UpdateUserDTO {
  @ApiModelProperty()
  full_name: string;

  @ApiModelProperty({ enum: Gender })
  gender: Gender;

  @ApiModelProperty()
  readonly department: string;

  @ApiModelProperty()
  readonly address: string;
}
