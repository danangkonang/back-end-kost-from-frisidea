import {
  IsNotEmpty,
  MaxLength,
  IsMobilePhone,
  IsEmail,
  IsEnum,
} from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

import { Gender, Role } from '../user.entity';

export class CreateUserDTO {
  @ApiModelProperty()
  @IsNotEmpty()
  readonly full_name: string;

  @ApiModelProperty()
  @IsEmail()
  readonly email: string;

  @ApiModelProperty()
  @IsMobilePhone('id-ID', { message: 'Please enter the right phone number' })
  @MaxLength(12, { message: 'Maximum length of phone number is 12' })
  readonly phone_number: string;

  @ApiModelProperty({ enum: Gender })
  @IsEnum(Gender)
  readonly gender: Gender;

  @ApiModelProperty({ enum: Role })
  @IsEnum(Role)
  readonly role: Role;

  @ApiModelProperty()
  @IsNotEmpty()
  readonly department: string;

  @ApiModelProperty()
  @IsNotEmpty()
  readonly kostIds: string[];
}
