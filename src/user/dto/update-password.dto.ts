import { ApiModelProperty } from '@nestjs/swagger';
import { MinLength, MaxLength, IsNotEmpty } from 'class-validator';

export class UpdatePasswordDTO {
  @ApiModelProperty()
  @IsNotEmpty()
  oldPassword: string;

  @ApiModelProperty()
  @MinLength(8)
  @MaxLength(16)
  newPassword: string;

  @ApiModelProperty()
  @MinLength(8)
  @MaxLength(16)
  confirmPassword: string;
}
