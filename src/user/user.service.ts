import {
  Injectable,
  HttpException,
  HttpStatus,
  UnauthorizedException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { validate } from 'class-validator';
import * as crypto from 'crypto';

import { User, Status, Role } from './user.entity';
import { Kost } from '../kost/kost.entity';
import SendGrid from '../common/services/sendgrid';
import { AuthLog } from '../auth/auth-log.entity';
import { BusinessInfo } from '../common/entities/business-info.entity';

import { UpdatePasswordDTO, UpdateUserDTO, CreateUserDTO } from './dto';
import { UpdateEmployeeDTO } from './dto/update-employee.dto';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User) private readonly userRepository: Repository<User>,
    @InjectRepository(Kost) private readonly kostRepository: Repository<Kost>,
    @InjectRepository(AuthLog)
    private readonly authLogRepository: Repository<AuthLog>,
    @InjectRepository(BusinessInfo)
    private readonly infoRepository: Repository<BusinessInfo>,
  ) {}

  async getAll(): Promise<object> {
    const data = await this.userRepository.find({ relations: ['auth_log'] });
    return { status: HttpStatus.OK, data };
  }

  async createEmployee(dto: CreateUserDTO, req: any) {
    const { email, phone_number } = dto;
    await this.checkAvailableUser(email, phone_number);

    const businessInfo = await this.infoRepository.findOne(
      { public_uid: req.user.business_info.public_uid },
      { relations: ['employees'] },
    );

    const newUser = new User();
    const authLog = new AuthLog();
    const password = await crypto.randomBytes(8).toString('hex');

    authLog.status = 'registered';

    newUser.full_name = dto.full_name;
    newUser.department = dto.department;
    newUser.email = dto.email;
    newUser.password = password;
    newUser.phone_number = dto.phone_number;
    newUser.gender = dto.gender;
    newUser.status = Status.COMPLETED;
    newUser.role = dto.role;
    newUser.auth_log = authLog;

    try {
      const kosts = dto.kostIds.map(id =>
        this.kostRepository.findOne({ public_uid: id }),
      );
      const userKosts = await Promise.all(kosts);
      newUser.kosts = userKosts;
    } catch (error) {
      throw new HttpException(
        { error: 'Kost was not found' },
        HttpStatus.NOT_FOUND,
      );
    }

    try {
      await validate(newUser);
      await newUser.hashPassword();
      await this.authLogRepository.save(authLog);
      const data = await this.userRepository.save(newUser);

      businessInfo.employees.push(data);
      await this.infoRepository.save(businessInfo);

      await SendGrid.sendLoginCredentials(newUser.email, password);
      return {
        status: HttpStatus.CREATED,
        data: "Login credentials has been sent to user's email",
      };
    } catch (error) {
      console.log(error);
      throw new HttpException({ error }, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  async updateEmployee(dto: UpdateEmployeeDTO, userId: string, req: any) {
    const user = await this.findOneByPuid(userId);

    if (user.role === Role.PIC && req.user.status !== Role.PIC) {
      throw new UnauthorizedException();
    }

    user.role = dto.role;

    if (dto.kostIds) {
      const temp = dto.kostIds.map(item => {
        return this.kostRepository.findOne({ public_uid: item });
      });
      const kosts = await Promise.all(temp);
      user.kosts = kosts;
    }

    try {
      await validate(user);
      return this.userRepository.save(user);
    } catch (error) {
      throw new HttpException({ error }, HttpStatus.BAD_REQUEST);
    }
  }

  async deleteEmployee(userId: string, req: any) {
    const user = await this.findOneByPuid(userId);

    if (user.role === Role.PIC && req.user.status !== Role.PIC) {
      throw new UnauthorizedException();
    }

    try {
      await this.userRepository.remove(user);
      return {
        status: HttpStatus.NO_CONTENT,
        message: 'User has been deleted',
      };
    } catch (error) {
      throw new HttpException({ error }, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  async updatePassword(dto: UpdatePasswordDTO, req: any): Promise<object> {
    const { newPassword, oldPassword, confirmPassword } = dto;

    const user = await this.userRepository.findOne({
      public_uid: req.user.public_uid,
    });

    if (!user) {
      throw new HttpException(
        { error: 'No user was found' },
        HttpStatus.NOT_FOUND,
      );
    }

    const match = await user.comparePassword(oldPassword);
    const validPassword = newPassword.match(
      /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])/,
    );

    if (!match) {
      throw new HttpException(
        { error: 'Wrong current password' },
        HttpStatus.BAD_REQUEST,
      );
    } else if (!validPassword) {
      throw new HttpException(
        {
          message:
            'Password must contain 1 Uppercase, 1 Special char and 1 Number',
        },
        HttpStatus.BAD_REQUEST,
      );
    } else if (newPassword === oldPassword) {
      throw new HttpException(
        { error: 'Your old password is the same as your new password' },
        HttpStatus.BAD_REQUEST,
      );
    } else if (newPassword !== confirmPassword) {
      throw new HttpException(
        { error: 'Your confirm password did not match' },
        HttpStatus.BAD_REQUEST,
      );
    } else {
      user.password = newPassword;
      await user.hashPassword();
      const data = await this.userRepository.save(user);
      return { status: HttpStatus.OK, data };
    }
  }

  async updateUser(dto: UpdateUserDTO, req: any): Promise<object> {
    const { full_name, gender } = dto;

    const user = await this.userRepository.findOne({
      public_uid: req.user.public_uid,
    });

    user.full_name = full_name;
    user.gender = gender;
    user.address = dto.address;

    if (req.file) {
      user.ktp_img = req.file.location;
    }

    try {
      const data = await this.userRepository.save(user);
      return { status: HttpStatus.OK, data };
    } catch (error) {
      throw new HttpException({ error }, HttpStatus.BAD_REQUEST);
    }
  }

  async me(userId: string) {
    const me = await this.userRepository
      .createQueryBuilder('user')
      .leftJoin('user.kosts', 'kosts')
      .where('user.public_uid = :userId', { userId })
      .select()
      .addSelect(['kosts.public_uid', 'kosts.name'])
      .getOne();

    return { status: HttpStatus.OK, data: me };
  }

  async findOneByPuid(public_uid: string): Promise<User> {
    const user = await this.userRepository.findOne(
      { public_uid },
      { relations: ['business_info'] },
    );

    if (!user) {
      throw new HttpException(
        { error: 'No user was found' },
        HttpStatus.NOT_FOUND,
      );
    }

    return user;
  }

  async checkAvailableUser(email: string, phoneNumber: string) {
    const user = await this.userRepository
      .createQueryBuilder('user')
      .where('user.phone_number = :phone_number', { phone_number: phoneNumber })
      .orWhere('user.email = :email', { email })
      .getOne();

    if (user) {
      const error = {
        error: 'Phone number or email had already been taken',
      };
      throw new HttpException(
        { message: 'Validation error', error },
        HttpStatus.BAD_REQUEST,
      );
    }

    return true;
  }
}
