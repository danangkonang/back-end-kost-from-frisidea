import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  PrimaryColumn,
  BeforeInsert,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToMany,
  OneToOne,
  JoinColumn,
  Unique,
  ManyToOne,
} from 'typeorm';

import * as bcrypt from 'bcryptjs';
import * as uuid from 'uuid';
import { Kost } from '../kost/kost.entity';
import { AuthLog } from '../auth/auth-log.entity';
import { BusinessInfo } from '../common/entities/business-info.entity';

export enum Gender {
  MALE = 'male',
  FEMALE = 'female',
}

export enum Role {
  PIC = 'person_in_charge',
  SUPER_ADMIN = 'super_admin',
  ADMIN = 'admin',
}

export enum Status {
  ACTIVATED = 'activated',
  NOT_ACTIVATED = 'not_activated',
  COMPLETED = 'completed',
  DISABLED = 'disabled',
}
@Entity()
@Unique(['email', 'phone_number'])
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @PrimaryColumn()
  public_uid: string;

  @Column()
  email: string;

  @Column()
  phone_number: string;

  @Column()
  otp: number;

  @Column({ nullable: true })
  full_name: string;

  @Column({ nullable: true })
  password: string;

  @Column({ type: 'enum', enum: Role, default: Role.PIC })
  role: Role;

  @Column({ type: 'enum', enum: Status, default: Status.NOT_ACTIVATED })
  status: Status;

  @Column({ type: 'enum', enum: Gender, nullable: true })
  gender: Gender;

  @Column({ nullable: true })
  ktp_img: string;

  @Column({ nullable: true })
  department: string;

  @Column({ type: 'text', nullable: true })
  address: string;

  @ManyToMany(() => Kost, kost => kost.users, { onDelete: 'SET NULL' })
  kosts: Kost[];

  @OneToOne(() => AuthLog, auth_log => auth_log.user, { onDelete: 'CASCADE' })
  @JoinColumn()
  auth_log: AuthLog;

  @ManyToOne(() => BusinessInfo, business => business.employees, {
    onDelete: 'CASCADE',
  })
  business_info: BusinessInfo;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  async hashPassword() {
    this.password = await bcrypt.hash(this.password, 10);
  }

  @BeforeInsert()
  generatedPublicUid() {
    this.public_uid = uuid()
      .split('-')
      .pop();
  }

  @BeforeInsert()
  generateOTP() {
    this.otp = Math.floor(Math.floor(100000 + Math.random() * 900000));
  }

  async comparePassword(userPass: string) {
    return await bcrypt.compare(userPass, this.password);
  }
}
