import { Module, NestModule, MiddlewareConsumer } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { UserController } from './user.controller';
import { User } from './user.entity';
import { Kost } from '../kost/kost.entity';
import { AuthLog } from '../auth/auth-log.entity';
import { BusinessInfo } from '../common/entities/business-info.entity';

import { UserService } from './user.service';
import { ImageUploadMiddleware } from '../common/middlewares/image-upload.middleware';

@Module({
  imports: [TypeOrmModule.forFeature([User, Kost, AuthLog, BusinessInfo])],
  controllers: [UserController],
  providers: [UserService],
  exports: [UserService],
})
export class UserModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(ImageUploadMiddleware).forRoutes(UserController);
  }
}
