import {
  Controller,
  Body,
  Get,
  Param,
  UseGuards,
  Put,
  HttpException,
  HttpStatus,
  Req,
  Post,
  Delete,
} from '@nestjs/common';
import { ApiBearerAuth } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';

import { UserService } from './user.service';
import { UpdatePasswordDTO, UpdateUserDTO, CreateUserDTO } from './dto';
import { UpdateEmployeeDTO } from './dto/update-employee.dto';
import { RolesGuard, StatusGuard } from '../common/guards';
import { Roles, Status } from '../common/decorators';
@Controller()
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Get('users')
  async getAll() {
    return this.userService.getAll();
  }

  @Get('me')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'), StatusGuard)
  @Status('completed', 'activated')
  async me(@Req() req: any) {
    return this.userService.me(req.user.public_uid);
  }

  @Put('me/update')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'), StatusGuard)
  @Status('completed', 'activated')
  async updateUser(@Body() dto: UpdateUserDTO, @Req() req: any) {
    try {
      return this.userService.updateUser(dto, req);
    } catch (error) {
      throw new HttpException({ error }, HttpStatus.BAD_REQUEST);
    }
  }

  @Post('users/create-employee')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'), RolesGuard, StatusGuard)
  @Status('completed')
  @Roles('person_in_charge', 'super_admin')
  async createUser(@Body() dto: CreateUserDTO, @Req() req: any) {
    return this.userService.createEmployee(dto, req);
  }

  @Get('users/:user_puid')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'), StatusGuard)
  @Status('completed')
  async getOne(@Param('user_puid') userPuid: string) {
    return this.userService.findOneByPuid(userPuid);
  }

  @Put('users/:user_puid/update-employee')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'), RolesGuard, StatusGuard)
  @Status('completed')
  @Roles('person_in_charge', 'super_admin')
  async updateEmployee(
    @Param('user_puid') userId: string,
    @Body() dto: UpdateEmployeeDTO,
    @Req() req: any,
  ) {
    return this.userService.updateEmployee(dto, userId, req);
  }

  @Delete('users/:user_puid/delete-employee')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'), RolesGuard, StatusGuard)
  @Status('completed')
  @Roles('person_in_charge', 'super_admin')
  async deleteEmployee(@Param('user_puid') userId: string, @Req() req: any) {
    return this.userService.deleteEmployee(userId, req);
  }

  @Put('/me/update-password')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'), StatusGuard)
  @Status('completed', 'activated')
  async updatePassword(@Body() dto: UpdatePasswordDTO, @Req() req: any) {
    return this.userService.updatePassword(dto, req);
  }
}
