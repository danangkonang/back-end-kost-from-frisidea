import { Module, NestModule, MiddlewareConsumer } from '@nestjs/common';
import { APP_GUARD } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';

import { KostController } from './kost.controller';
import { KostService } from './kost.service';
import { Kost } from './kost.entity';
import { User } from '../user/user.entity';
import { ContractHistory } from '../contract/contract-history.entity';

import { ImagesUploadMiddleware } from '../common/middlewares/images-upload.middleware';

import { UserModule } from '../user/user.module';
import { BusinessInfo } from '../common/entities/business-info.entity';
import { Room } from '../room/room.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([Kost, User, ContractHistory, Room, BusinessInfo]),
    UserModule,
  ],
  controllers: [KostController],
  providers: [KostService],
  exports: [KostService],
})
export class KostModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(ImagesUploadMiddleware).forRoutes(KostController);
  }
}
