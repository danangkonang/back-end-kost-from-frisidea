import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { validate } from 'class-validator';

import { Kost, KostStatus } from './kost.entity';
import { User } from '../user/user.entity';
import { ContractHistory } from '../contract/contract-history.entity';
import { BusinessInfo } from '../common/entities/business-info.entity';

import { UserService } from '../user/user.service';
import { CreateKostDTO, UpdateKostDTO } from './dto';
import { IKostQuery, IEmployeesQuery } from '../common/interfaces';
import { Room, RoomStatus } from '../room/room.entity';

@Injectable()
export class KostService {
  constructor(
    @InjectRepository(Kost) private readonly kostRepository: Repository<Kost>,
    @InjectRepository(User) private readonly userRepository: Repository<User>,
    @InjectRepository(ContractHistory)
    private readonly historyRepository: Repository<ContractHistory>,
    @InjectRepository(BusinessInfo)
    private readonly infoRepository: Repository<BusinessInfo>,
    @InjectRepository(Room) private readonly roomRepository: Repository<Room>,
    private readonly userService: UserService,
  ) {}

  async getAll(query: IKostQuery, userId: string) {
    const take = query.take || 5;
    const skip = query.skip || 0;

    try {
      const data = await this.kostRepository
        .createQueryBuilder('kost')
        .leftJoin('kost.users', 'users')
        .leftJoin('kost.room_types', 'room_types')
        .select()
        .addSelect(['room_types.public_uid', 'room_types.name'])
        .where('users.public_uid = :userId', { userId })
        .andWhere('kost.kost_status = :status', { status: query.status })
        .orderBy('kost.createdAt', query.order)
        .take(take)
        .skip(skip)
        .getMany();

      return { status: HttpStatus.OK, data };
    } catch (error) {
      throw new HttpException({ error }, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  async getOne(userId: string, req: any) {
    const data = await this.findById(userId, req);
    if (!data) {
      throw new HttpException(
        { error: 'Kost not found' },
        HttpStatus.NOT_FOUND,
      );
    }
    return { status: HttpStatus.OK, data };
  }

  async getUsers(kostId: string) {
    const data = await this.userRepository
      .createQueryBuilder('user')
      .leftJoin('user.kosts', 'kosts')
      .where('kosts.public_uid = :kostId', { kostId })
      .getMany();

    return { status: HttpStatus.OK, data };
  }

  async getAllUsers(businessInfoId: string, query: IEmployeesQuery) {
    const data = this.infoRepository
      .createQueryBuilder('business_info')
      .leftJoinAndSelect('business_info.employees', 'users')
      .where('business_info.public_uid = :businessInfoId', { businessInfoId })
      .orderBy('users.createdAt', query.order);

    if (query.search) {
      const newData = await data
        .andWhere('users.full_name like :name', {
          name: '%' + query.search + '%',
        })
        .getOne();

      if (!newData) {
        return {
          status: HttpStatus.NOT_FOUND,
          data: 'No employees was found',
        };
      }

      return { status: HttpStatus.OK, data: newData.employees };
    }

    const { employees } = await data.getOne();
    return { status: HttpStatus.OK, data: employees };
  }

  async create(dto: CreateKostDTO, req: any) {
    const user: User = await this.userService.findOneByPuid(
      req.user.public_uid,
    );

    const businessInfo = await this.infoRepository.findOne(
      {
        id: user.business_info.id,
      },
      { relations: ['kosts'] },
    );

    let kost = new Kost();

    kost.name = dto.name;
    kost.address = dto.address;
    kost.type = dto.type;
    kost.visit_hours = dto.visit_hours;
    kost.max_hours_visit = dto.max_hours_visit;
    kost.pets_policy = dto.pets_policy;
    kost.province = dto.province;
    kost.city = dto.city;
    kost.district = dto.district;
    kost.sub_district = dto.sub_district;
    kost.postal_code = dto.postal_code;
    kost.users = [user];
    if (req.files) {
      kost.kost_imgs = req.files.map((item: any) => item.location);
    }

    try {
      await validate(kost);
      const data = await this.kostRepository.save(kost);

      businessInfo.kosts.push(data);
      await this.infoRepository.save(businessInfo);

      return { status: HttpStatus.CREATED, data };
    } catch (error) {
      console.log(error);
      throw new HttpException({ error }, HttpStatus.BAD_REQUEST);
    }
  }

  async update(dto: UpdateKostDTO, kostId: string, req: any) {
    const kost = await this.findById(kostId, req);

    if (!kost) {
      throw new HttpException(
        { error: 'Kost not found' },
        HttpStatus.NOT_FOUND,
      );
    }

    kost.name = dto.name;
    kost.address = dto.address;
    kost.type = dto.type;
    kost.visit_hours = dto.visit_hours;
    kost.max_hours_visit = dto.max_hours_visit;
    kost.pets_policy = dto.pets_policy;
    kost.province = dto.province;
    kost.city = dto.city;
    kost.district = dto.district;
    kost.sub_district = dto.sub_district;
    kost.postal_code = dto.postal_code;
    kost.kost_status = dto.kost_status;
    if (req.files) {
      kost.kost_imgs = req.files.map((item: any) => {
        kost.kost_imgs.push(item.location);
      });
    }

    try {
      await validate(kost);
      const data = await this.kostRepository.save(kost);
      return { status: HttpStatus.OK, data };
    } catch (error) {
      throw new HttpException({ error }, HttpStatus.BAD_REQUEST);
    }
  }

  async delete(kostId: string, req: any) {
    const kost = await this.findById(kostId, req);

    if (!kost) {
      throw new HttpException(
        { error: 'Kost not found' },
        HttpStatus.NOT_FOUND,
      );
    }

    try {
      await this.kostRepository.remove(kost);
      return {
        status: HttpStatus.NO_CONTENT,
        data: 'Kost has been successfully deleted',
      };
    } catch (error) {
      throw new HttpException({ error }, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  async findById(kostId: string, req: any) {
    try {
      const kost = await this.kostRepository
        .createQueryBuilder('kost')
        .leftJoin('kost.users', 'users')
        .where('kost.public_uid = :kostId', { kostId })
        .andWhere('users.public_uid = :userId', {
          userId: req.user.public_uid,
        })
        .andWhere('kost.kost_status = :status', { status: KostStatus.ACTIVE })
        .getOne();

      if (!kost) {
        throw new HttpException(
          { error: 'No Kost was found' },
          HttpStatus.NOT_FOUND,
        );
      }
      return kost;
    } catch (error) {
      throw new HttpException({ error }, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  async findByIdRaw(kostId: string) {
    const kost = await this.kostRepository
      .createQueryBuilder('kost')
      .where('kost.public_uid = :kostId', { kostId })
      .getRawOne();

    if (!kost) {
      throw new HttpException(
        { error: 'No Kost was found' },
        HttpStatus.NOT_FOUND,
      );
    }

    return kost;
  }

  async findContractHistory(kostId: string) {
    const data = await this.historyRepository
      .createQueryBuilder('history')
      .where('history.kost_puid = :kostId', { kostId })
      .getMany();

    if (!data) {
      throw new HttpException(
        { error: 'No Contract History' },
        HttpStatus.NOT_FOUND,
      );
    }

    return { status: HttpStatus.OK, data };
  }

  async findForReservation(kostId: string) {
    const { AVAILABLE, NOT_AVAILABLE, PARTIAL_AVAILABLE } = RoomStatus;

    const data = await this.roomRepository
      .createQueryBuilder('room')
      .leftJoin('room.room_type', 'room_type')
      .leftJoinAndSelect('room.contract', 'contract')
      .where('room.kost_puid = :kostId', { kostId })
      .andWhere('room.status IN (:...status)', {
        status: [AVAILABLE, NOT_AVAILABLE, PARTIAL_AVAILABLE],
      })
      .select()
      .addSelect([
        'contract.public_uid',
        'contract.contract_start',
        'contract.contract_end',
        'contract.payment_status',
        'contract.customer_name',
      ])
      .addSelect(['room_type.public_uid', 'room_type.name'])
      .getMany();

    if (!data) {
      throw new HttpException(
        { error: 'No reservation was found' },
        HttpStatus.BAD_REQUEST,
      );
    }

    return data;
  }
}
