import {
  Entity,
  PrimaryGeneratedColumn,
  PrimaryColumn,
  Column,
  BeforeInsert,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToMany,
  JoinTable,
  OneToMany,
  OneToOne,
  JoinColumn,
  ManyToOne,
} from 'typeorm';

import * as uuid from 'uuid';

import { User } from '../user/user.entity';
import { RoomType } from '../room-type/room-type.entity';
import { Customer } from '../customer/customer.entity';
import { BusinessInfo } from '../common/entities/business-info.entity';

export enum Type {
  MEN = 'men',
  WOMEN = 'women',
  MUSLIM_MEN = 'muslim_men',
  MUSLIM_WOMEN = 'muslim_women',
  MIX = 'mix',
}

export enum VisitHours {
  LIMITED = 'limited',
  FREE = 'free',
}

export enum PetsPolicy {
  ALLOW = 'allow',
  NOT_ALLOW = 'not_allow',
}

export enum KostStatus {
  ACTIVE = 'active',
  INACTIVE = 'inactive',
}

@Entity()
export class Kost {
  @PrimaryGeneratedColumn()
  id: number;

  @PrimaryColumn()
  public_uid: string;

  @Column({ type: 'enum', enum: KostStatus, default: KostStatus.ACTIVE })
  kost_status: KostStatus;

  @Column()
  name: string;

  @Column()
  address: string;

  @Column({ type: 'enum', enum: Type })
  type: Type;

  @Column({ type: 'enum', enum: VisitHours, nullable: true })
  visit_hours: VisitHours;

  @Column({ nullable: true })
  max_hours_visit: string;

  @Column({ type: 'enum', enum: PetsPolicy, nullable: true })
  pets_policy: PetsPolicy;

  @Column({ nullable: true })
  province: string;

  @Column({ nullable: true })
  city: string;

  @Column({ nullable: true })
  district: string;

  @Column({ nullable: true })
  sub_district: string;

  @Column({ nullable: true })
  postal_code: string;

  @Column('text', { array: true, nullable: true })
  kost_imgs: string[];

  @ManyToMany(() => User, user => user.kosts, { onDelete: 'SET NULL' })
  @JoinTable()
  users: User[];

  @OneToMany(() => Customer, customer => customer.kost, {
    onDelete: 'SET NULL',
  })
  customer: Customer[];

  @OneToMany(() => RoomType, room_type => room_type.kost, {
    onDelete: 'SET NULL',
  })
  room_types: RoomType[];

  @ManyToOne(() => BusinessInfo, business => business.kosts, {
    onDelete: 'CASCADE',
  })
  business_info: BusinessInfo;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @BeforeInsert()
  generatedPublicUid() {
    this.public_uid = uuid()
      .split('-')
      .pop();
  }
}
