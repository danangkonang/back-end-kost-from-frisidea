import {
  Controller,
  Post,
  UseGuards,
  Body,
  Req,
  Get,
  Put,
  Param,
  Query,
  Delete,
  SetMetadata,
} from '@nestjs/common';

import { KostService } from './kost.service';
import { ApiBearerAuth, ApiImplicitQuery } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';

import { CreateKostDTO, UpdateKostDTO } from './dto';
import { IKostQuery, IEmployeesQuery } from '../common/interfaces';
import { StatusGuard } from '../common/guards';
import { Status } from '../common/decorators';
@Controller('kosts')
export class KostController {
  constructor(private readonly kostService: KostService) {}

  @Get()
  @ApiBearerAuth()
  @ApiImplicitQuery({ name: 'skip', required: false })
  @ApiImplicitQuery({ name: 'take', required: false })
  @ApiImplicitQuery({ name: 'order', required: false })
  @ApiImplicitQuery({ name: 'status', required: true })
  @UseGuards(AuthGuard('jwt'), StatusGuard)
  @Status('completed', 'activated')
  async getAll(@Query() query: IKostQuery, @Req() req: any) {
    return this.kostService.getAll(query, req.user.public_uid);
  }

  @Post()
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'), StatusGuard)
  @Status('completed')
  async create(@Body() dto: CreateKostDTO, @Req() req: any) {
    return this.kostService.create(dto, req);
  }

  @Get('users')
  @ApiBearerAuth()
  @ApiImplicitQuery({ name: 'search', required: false })
  @ApiImplicitQuery({ name: 'order', required: false })
  @UseGuards(AuthGuard('jwt'), StatusGuard)
  @Status('completed', 'activated')
  async getAllUsers(
    @Req() { user: { business_info } }: any,
    @Query() query: IEmployeesQuery,
  ) {
    return this.kostService.getAllUsers(business_info.public_uid, query);
  }

  @Get(':kost_puid')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'), StatusGuard)
  @Status('completed', 'activated')
  async getOne(@Param('kost_puid') kostId: string, @Req() req: any) {
    return this.kostService.getOne(kostId, req);
  }

  @Get(':kost_puid/users')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'), StatusGuard)
  @Status('completed', 'activated')
  async getUsers(@Param('kost_puid') kostId: string) {
    return this.kostService.getUsers(kostId);
  }

  @Get(':kost_puid/contract-history')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'), StatusGuard)
  @Status('completed', 'activated')
  async getContractHistory(@Param('kost_puid') kostId: string) {
    return this.kostService.findContractHistory(kostId);
  }

  @Get(':kost_puid/room-reservations')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'), StatusGuard)
  @Status('completed', 'activated')
  async getRoomReservation(@Param('kost_puid') kostId: string) {
    return this.kostService.findForReservation(kostId);
  }

  @Put(':kost_puid/update')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'), StatusGuard)
  @Status('completed')
  async update(
    @Body() dto: UpdateKostDTO,
    @Param('kost_puid') kostId: string,
    @Req() req: any,
  ) {
    return this.kostService.update(dto, kostId, req);
  }

  @Delete(':kost_puid/delete')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'), StatusGuard)
  @Status('completed', 'activated')
  async delete(@Param('kost_puid') kostId: string, @Req() req: any) {
    return this.kostService.delete(kostId, req);
  }
}
