import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsEnum } from 'class-validator';

import { Type, VisitHours, PetsPolicy } from '../../kost/kost.entity';

export class CreateKostDTO {
  @ApiModelProperty()
  @IsNotEmpty()
  readonly name: string;

  @ApiModelProperty()
  @IsNotEmpty()
  readonly address: string;

  @ApiModelProperty({ enum: Type })
  @IsEnum(Type)
  readonly type: Type;

  @ApiModelProperty({ enum: VisitHours })
  @IsEnum(VisitHours)
  readonly visit_hours: VisitHours;

  @ApiModelProperty()
  readonly max_hours_visit: string;

  @ApiModelProperty({ enum: PetsPolicy })
  @IsEnum(PetsPolicy)
  readonly pets_policy: PetsPolicy;

  @ApiModelProperty()
  @IsNotEmpty()
  readonly province: string;

  @ApiModelProperty()
  @IsNotEmpty()
  readonly city: string;

  @ApiModelProperty()
  @IsNotEmpty()
  readonly district: string;

  @ApiModelProperty()
  @IsNotEmpty()
  readonly sub_district: string;

  @ApiModelProperty()
  @IsNotEmpty()
  readonly postal_code: string;
}
