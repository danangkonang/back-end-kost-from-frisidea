import { ApiModelProperty } from '@nestjs/swagger';

import {
  Type,
  VisitHours,
  PetsPolicy,
  KostStatus,
} from '../../kost/kost.entity';

export class UpdateKostDTO {
  @ApiModelProperty()
  readonly name: string;

  @ApiModelProperty()
  readonly address: string;

  @ApiModelProperty({ enum: Type })
  readonly type: Type;

  @ApiModelProperty({ enum: VisitHours })
  readonly visit_hours: VisitHours;

  @ApiModelProperty()
  readonly max_hours_visit: string;

  @ApiModelProperty({ enum: PetsPolicy })
  readonly pets_policy: PetsPolicy;

  @ApiModelProperty()
  readonly province: string;

  @ApiModelProperty()
  readonly city: string;

  @ApiModelProperty()
  readonly district: string;

  @ApiModelProperty()
  readonly sub_district: string;

  @ApiModelProperty()
  readonly postal_code: string;

  @ApiModelProperty({ enum: KostStatus })
  readonly kost_status: KostStatus;
}
